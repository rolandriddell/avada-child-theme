<?php
/**
 * Header-1 template.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       http://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
if(session_id() == '')
{
     session_start();
}
if ( is_user_logged_in() ) {
    $notificationcount=0;
    $showNotification=array();
global $wpdb;
$nfuser=get_current_user_id();
$sql_notify = "SELECT `post`.`ID`,post.`post_status`,post.`post_type` FROM ".$wpdb->prefix."posts as post
INNER JOIN ".$wpdb->prefix."postmeta as meta
ON post.`ID`= `meta`.`post_id`
WHERE `meta`.`meta_key`='_customer_user' AND `meta`.`meta_value`='$nfuser' AND post.`post_type`='shop_order'";
//echo $sql_notify;
$result_nf = $wpdb->get_results($sql_notify);
foreach ($result_nf as $row) {
    $childArray=array();
    $orderSubmitQuery="SELECT post.`ID`,post.`post_status` FROM ".$wpdb->prefix."posts as post
    INNER JOIN ".$wpdb->prefix."postmeta as meta
    ON post.`ID`= meta.post_id
    WHERE post.`ID`='$row->ID' AND post.`post_type`='shop_order' LIMIT 1";
    $result_nf1 = $wpdb->get_results($orderSubmitQuery);
    $cq="SELECT `post_id` FROM ".$wpdb->prefix."postmeta WHERE `meta_key`= '_orderplace_nf' AND `meta_value`='1' AND post_id='$row->ID'";
    $cqre = $wpdb->get_results($cq);
    if (count($result_nf1)> 0 && $wpdb->num_rows<1){
        $childArray['_orderplace_nf']="Order Placed";
        $notificationcount++;
    }
    $orderSubmitQuery1="SELECT post.`ID`,post.`post_status` FROM ".$wpdb->prefix."posts as post
    INNER JOIN ".$wpdb->prefix."postmeta as meta
    ON post.`ID`= meta.post_id
    WHERE post.`ID`='$row->ID' AND post.`post_status`='wc-completed' LIMIT 1";
    $result_nf2 = $wpdb->get_results($orderSubmitQuery1);
    $cq="SELECT `post_id` FROM ".$wpdb->prefix."postmeta WHERE `meta_key`= '_ordercomplete_nf' AND `meta_value`='1' AND post_id='$row->ID'";
    $cqre = $wpdb->get_results($cq);
    if (count($result_nf2)> 0 && $wpdb->num_rows<1){
        $childArray['_ordercomplete_nf']="Order Completed";
        $notificationcount++;
    }
    $orderSubmitQuery2="SELECT nform.`order_id`,nform.`status`  FROM ".$wpdb->prefix."notary_form as nform
    INNER JOIN ".$wpdb->prefix."postmeta as meta ON nform.`order_id` = meta.post_id
    WHERE nform.`order_id`='$row->ID' AND nform.`user_id`='$nfuser' AND nform.`status`='1' AND meta.meta_key!='_notarysubmitted_nf' AND meta.meta_value!='1' LIMIT 1";
    $result_nf3 = $wpdb->get_results($orderSubmitQuery2);
    $cq="SELECT `post_id` FROM ".$wpdb->prefix."postmeta WHERE `meta_key`= '_notarysubmitted_nf' AND `meta_value`='1' AND post_id='$row->ID'";
    $cqre = $wpdb->get_results($cq);
    if (count($result_nf3)> 0 && $wpdb->num_rows<1){
        $childArray['_notarysubmitted_nf']="Notary Form Submitted";
        $notificationcount++;
    }
    $orderSubmitQuery3="SELECT nform.`order_id`,nform.`status`  FROM ".$wpdb->prefix."notary_form as nform
    INNER JOIN ".$wpdb->prefix."postmeta as meta ON nform.`order_id` = meta.post_id
    WHERE nform.`order_id`='$row->ID
    ' AND nform.`user_id`='$nfuser' AND nform.`status`='2' AND meta.meta_key!='_notarycompleted_nf' AND meta.meta_value!='1' LIMIT 1";
    $result_nf4 = $wpdb->get_results($orderSubmitQuery3);
    $cq="SELECT `post_id` FROM ".$wpdb->prefix."postmeta WHERE `meta_key`= '_notarycompleted_nf' AND `meta_value`='1' AND post_id='$row->ID'";
    $cqre = $wpdb->get_results($cq);
    if (count($result_nf4)> 0 && $wpdb->num_rows<1){
        $childArray['_notarycompleted_nf']="Notary Form Completed";
        $notificationcount++;
    }
    $currentdate=date('Y-m-d');//
    $cdate=date('Y-m-d', strtotime("+4 year $currentdate"));
    $orderSubmitQuery4="SELECT `order_id` FROM `".$wpdb->prefix."expire_plan` WHERE `order_id`='$row->ID' AND `trash`!=1 AND (`expire_date` LIKE('$currentdate%') OR expire_date<'$currentdate' )";
    $result_nf5 = $wpdb->get_results($orderSubmitQuery4);
    $cq="SELECT `post_id` FROM ".$wpdb->prefix."postmeta WHERE `meta_key`= '_notaryexpired_nf' AND `meta_value`='1' AND post_id='$row->ID'";
    $cqre = $wpdb->get_results($cq);
    if (count($result_nf5)> 0 && $wpdb->num_rows<1){
         $childArray['_notaryexpired_nf']="Notary Form Expired";
        $notificationcount++;
    }
    $orderSubmitQuery5="SELECT `ID` FROM `".$wpdb->prefix."posts` WHERE `post_parent`='$row->ID' AND `post_type`='wc_stamps_label' ORDER BY ID DESC LIMIT 1";
    $result_nf6 = $wpdb->get_results($orderSubmitQuery5);
    $cq="SELECT `post_id` FROM ".$wpdb->prefix."postmeta WHERE `meta_key`= '_notaryshiped_nf' AND `meta_value`='1' AND post_id='$row->ID'";
    $cqre = $wpdb->get_results($cq);
    if (count($result_nf6)> 0 && $wpdb->num_rows<1){
        $childArray['_notaryshiped_nf']="Order Shiped";
        $notificationcount++;
    }





    $showNotification[$row->ID]=$childArray;
    //echo "<pre>";print_r($childArray);echo "</pre>";
}
//echo "<pre>";print_r($showNotification);echo "</pre>";
}
?>
<!--<div id="preloader"></div>-->
<div class="fusion-header-sticky-height"></div>
<div class="fusion-header">
	<div class="fusion-row">
		<?php avada_logo(); ?>
        <div class="right-header">
            <?php avada_main_menu(); ?>
             <?php if(!empty(WC()->cart->get_cart_contents_count())){

			 ?>
            <div class="view-cart">
                <a href="<?php echo esc_url( wc_get_cart_url() ); ?>" id="cart-link-main"  class="button wc-forward"> <?php _e( 'Cart', 'woocommerce' ); ?><?php if(!empty(WC()->cart->get_cart_contents_count())){ ?><span class="counter" cart-total = "<?php echo WC()->cart->get_cart_contents_count(); ?>"><?php echo WC()->cart->get_cart_contents_count(); ?></span><?php } ?></a>
            </div>
            <?php  } ?>
            <div class="login-btn">
                 <?php if ( is_user_logged_in() ) { ?>
                    <a class="myAccount" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Dashboard','woothemes'); ?>"><?php _e('Dashboard','woothemes'); ?></a>

                    <a class="logoutBtn" href="<?php echo wp_logout_url(get_permalink());  ?>" title="<?php _e('Logout','woothemes'); ?>"><?php _e('Logout','woothemes'); ?></a>
                 <?php }
                 else { ?>
                    <a class="login" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>"><?php _e('Login','woothemes'); ?></a>
                <?php } ?>
            </div>
            <?php if(!empty($showNotification) && $notificationcount>0){ ?>
            <div class="notification custom-bell-iconNotification">
              <a id="notification"><i class="fa fa-bell-o"></i><span class="counter" id="notifyCounter" data-id="<?php echo $notificationcount; ?>"><?php echo $notificationcount; ?></span></a>
              <div style="display: none;" class="dropdown-menu">
                <ul>
                <?php foreach ($showNotification as $key => $value) {
                    foreach ($value as $k => $v) { ?>
                        <li><a href="javascript:;" data-id="<?php echo $key; ?>" data-value="<?php echo $k; ?>" onclick="updateNF(this);"><?php echo "#".$key." ". $v; ?></a></li>
                <?php }}  ?>
                </ul>
              </div>
            </div>
            <?php } ?>
            <div class="clear"></div>
        </div>
	</div>
</div>
<?php global $template;
 if(isset($_COOKIE['gpackagesbtn']) && basename($template) !="GroupPurchase.php" && basename($template) !="GroupCart.php" && basename($template) !="WoocommerceCustomTemplate.php") {
$cookie_value=$_COOKIE["gpackagesbtn"];
    ?>
<a href="<?php echo get_site_url();?>/group-purchase/?id=<?php echo $cookie_value; ?>" class="editIcon bounce" id="unsetcookie">
	<span class="icon"><img src="<?php echo get_site_url();?>/wp-content/themes/Avada-Child-Theme/images/edit-form.png" /></span>
	<span class="showTitle">Your Package</br>is Waiting</span>
</a>
<?php } ?>
<script>
jQuery(document).ready(function(){
   jQuery(document).on('click','.remove_package',function(){
        setTimeout(function(){
            var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
            var getdataalert=0;
            jQuery.ajax({
                url : ajaxurl,
                type : 'post',
                data : {
                    action : 'get_exect_counter_cart'
                },
                async:false,
                success : function( response ) {
                    getdataalert=response;
                }
            });
            if(getdataalert>0)
            {
                jQuery(".counter").attr("cart-total",getdataalert);
                jQuery(".counter").html(getdataalert);
            }
            else
            {
                jQuery(".counter").remove();
            }
        }, 1000);
    });

});
function updateNF(elm)
{
    var nfcnumber=jQuery("#notifyCounter").attr("data-id");
    var nf_id=jQuery(elm).attr("data-id");
    var nf_key=jQuery(elm).attr("data-value");
    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
    var getdataalert=0;
    var newcnumber=nfcnumber-1;
    jQuery.ajax({
        url : ajaxurl,
        type : 'post',
        data : {
            action : 'updateNfunction',
            id : nf_id,
            value : nf_key,

        },
        async:false,
        context:elm,
        success : function( response ) {
            /*console.log(response); */
            if(newcnumber>0){
                jQuery('#notifyCounter').attr("data-id",newcnumber);
                jQuery('#notifyCounter').html(newcnumber);
                jQuery(elm).parent().remove();
            }
            else
            {
            	jQuery(".custom-bell-iconNotification").remove();
            }
        }
    });

}
</script>
<?php

if(basename(get_permalink())==="checkout")
{ ?>
    <style>.woocommerce  ul.woocommerce-error {display: none;}</style>
<?php } ?>

