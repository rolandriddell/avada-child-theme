jQuery('#ssn_number').inputmask('999-99-9999',{placeholder:'XXX-XX-XXXX'});
jQuery("#current_dates").inputmask('99-99-9999');
jQuery("#dob").inputmask('99-99-9999');
jQuery("#zipcode").inputmask('99999');


jQuery(document).ready(function(){
	jQuery('.extraInfo').hide();
	jQuery('#mnConform').click(function(){
		if(jQuery(this).is(":checked")){
			jQuery('.extraInfo').show();
		}else{
			jQuery('.extraInfo').hide(); 
		}
	});
	
	jQuery("#shape,#color").prepend(new Option('Choose an option', '', true, true));
	jQuery("#shape option:first").attr("selected", "selected");
	jQuery('#notary_id,#notary_id_number').attr('minlength','9');
	jQuery('#notary_id,#notary_id_number').attr('maxlength','9');
	jQuery("#notary_id,#notary_id_number,.number_validation").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl/cmd+A
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: Ctrl/cmd+C
			(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: Ctrl/cmd+X
			(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	jQuery("#notarystampform").submit(function(){
		console.log(jQuery("#notary_id_number").val().length);
		jQuery(".nformError").remove();
		var check=0;
		if(jQuery("#notary_commission_name").val()=="")
		{
			jQuery("#notary_commission_name").after("<span class='error nformError'>Please enater commission name.</span>");
			check=1;
		}
		if(jQuery("#notary_id_number").val()=="")
		{
			jQuery("#notary_id_number").after("<span class='error nformError'>Please enater Notary ID.</span>");
			check=1;
		}
		if(jQuery("#notary_id_number").val().length!=9 && jQuery("#notary_id_number").val()!="")
		{
			jQuery("#notary_id_number").after("<span class='error nformError'>Notary ID number shuld be 9 digit.</span>");
			check=1;
		}
		if(jQuery("#notary_expiration_date").val()=="")
		{
			jQuery("#notary_expiration_date").after("<span class='error nformError'>Please enater expiration date.</span>");
			check=1;
		}
		if(jQuery("#selec_color").val()=="")
		{
			jQuery("#selec_color").after("<span class='error nformError'>Please slect Notary color.</span>");
			check=1;
		}
		if(jQuery("#selec_shape").val()=="")
		{
			jQuery("#selec_shape").after("<span class='error nformError'>Please Notary shape.</span>");
			check=1;
		}
		if(check==1)
		{
			return false;
		}
	});
	
});
jQuery("#expiration_date,#notary_expiration_date,.notary_expiration_date").inputmask('99-99-9999');
jQuery( function($) {
	if(typeof($('#expiration_date,#notary_expiration_date,.notary_expiration_date').datepicker)=="function")
	{
		$('#expiration_date,#notary_expiration_date,.notary_expiration_date').datepicker({
			dateFormat : 'mm-dd-yy',
			changeMonth : true,
			changeYear : true,
			minDate: 0
	   	});
	}
	
}); 