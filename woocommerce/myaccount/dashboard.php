<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<?php
if ( is_user_logged_in() && isset($_GET['redirect']) && $_GET['redirect']!='' ) {
	echo $_GET['redirect'];
	header("Location:  ".$_GET['redirect']);
	}
?>
<p><?php
	/* translators: 1: user display name 2: logout url */
	printf(
		__( 'Hello %1$s (not %1$s? <a href="%2$s">Log out</a>)', 'woocommerce' ),
		'<strong>' . esc_html( $current_user->display_name ) . '</strong>',
		esc_url( wc_logout_url( wc_get_page_permalink( 'myaccount' ) ) )
	);
?></p>

<p><?php
	printf(
		__( 'From your account dashboard you can view your <a href="%1$s">recent orders</a>, manage your <a href="%2$s">shipping and billing addresses</a> and <a href="%3$s">edit your password and account details</a>.', 'woocommerce' ),
		esc_url( wc_get_endpoint_url( 'orders' ) ),
		esc_url( wc_get_endpoint_url( 'edit-address' ) ),
		esc_url( wc_get_endpoint_url( 'edit-account' ) )
	);
?></p>



<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */

	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
?>
<div>
	<!-- Get Application complete button for single purchase and group purchase  -->
<?php
	$get_plan_product="";
	// WP_Query arguments
	$arg_plan = array (
	    'post_type'              => array( 'product' ),
	    'post_status'            => array( 'publish' ),
	    'meta_query'             => array(
	        array(
	            'key'       => 'wpcf-product_type',
	            'value'     => 'plan',
	        ),
	    ),
	);

	$plan_query = new WP_Query( $arg_plan );

	// The Loop
	if ( $plan_query->have_posts() ) {
	    while ( $plan_query->have_posts() ) {
	        $plan_query->the_post();
	        if($get_plan_product==""){
	        	$get_plan_product = get_the_ID();
	        }
	        else
	        {
	        	$get_plan_product=$get_plan_product.",".get_the_ID();
	        }
	    }
	}
	global $wpdb;
	$getincompletebutton=0;
	$userInfo=get_current_user_id();
	$currn_id1=$currn_id=$cust_name=$package_id="";
	$userInfo1=$userInfo;
	if($get_plan_product!="")
	{
		$getpost_id=$wpdb->get_row("SELECT post.`ID`, order_itemmeta.meta_value, order_item.order_item_name FROM `wp_posts` as post INNER JOIN wp_postmeta as meta ON post.ID=meta.post_id INNER JOIN wp_woocommerce_order_items as order_item ON post.`ID`= order_item.`order_id` INNER JOIN wp_woocommerce_order_itemmeta as order_itemmeta ON order_item.`order_item_id`= order_itemmeta.order_item_id WHERE meta.`meta_key` = '_customer_user' AND order_itemmeta.meta_key='_product_id' AND order_itemmeta.meta_value IN($get_plan_product) AND meta.`meta_value` = '".$userInfo."' AND post.post_type = 'shop_order' ORDER BY post.ID desc limit 1");

		if(isset($getpost_id->ID))
		{
			$currn_id =  $getpost_id->ID;
			$currn_id1= $getpost_id->ID;
			$cust_name = $getpost_id->order_item_name;
			$package_id = $getpost_id->meta_value;
		}
	}
	if($currn_id1=="")
	{

		$newcquery=$wpdb->get_row("SELECT `order_id` FROM `wp_group_purchase` WHERE `user_id`='$userInfo' AND `parent_id`!=0 ORDER BY `id` DESC LIMIT 1");
		if(isset($newcquery->order_id))
		{
			$currn_id1 = $newcquery->order_id;

		}
		$newuquery=$wpdb->get_row("SELECT `user_id` FROM `wp_group_purchase` WHERE `order_id`='$currn_id1' AND `parent_id`=0");
		if(isset($newuquery->user_id))
		{
			$userInfo1 = $newuquery->user_id;

		}
	}
	$getneworder = $wpdb->get_results("SELECT `wp_posts`.`ID` ,`wp_postmeta`.`meta_value` FROM `wp_posts` LEFT JOIN `wp_postmeta` ON `wp_posts`.`ID` = `wp_postmeta`.`post_id` INNER JOIN(SELECT `o_id` FROM `wp_single_user_package_detail` UNION SELECT `order_id` FROM `wp_group_purchase` WHERE parent_id=0 ) as `wps` ON `wp_posts`.`ID` = `wps`.`o_id` WHERE `wp_postmeta`.`meta_value` = '".$userInfo1."' AND `wp_posts`.`post_type`='shop_order' AND `wp_posts`.`ID`NOT IN(SELECT `order_id` FROM `wp_notary_form` WHERE `user_id`='$userInfo') AND `wp_posts`.`ID`='$currn_id1'");

	$getcount=$wpdb->num_rows;
	if($getcount>0){

		$getgrouporder=$wpdb->get_results("SELECT * FROM `wp_group_purchase` WHERE `order_id`='".$getneworder[0]->ID."'");
		$group_count=$wpdb->num_rows;
		if($group_count>0){

			$getgrouporder1=$wpdb->get_results("SELECT * FROM `wp_group_purchase` WHERE `order_id`='".$getneworder[0]->ID."' AND `user_id`='".$userInfo."' AND add_product IS NOT NULL");
			$pro_count=$wpdb->num_rows;

			if($pro_count>0){
				$getincompletebutton=1;
			?>
			<a href="<?php echo get_site_url(); ?>/group-notary-application-form/?key=<?php echo base64_encode($getneworder[0]->ID); ?>" class="btn">Complete The Notary Application</a>
		<?php } }else{ $getincompletebutton=1;
			?>
			<a href="<?php echo get_site_url(); ?>/notary-application-form?o_id=<?php echo base64_encode($getneworder[0]->ID); ?>" class="btn">Complete The Notary Application</a>
	<?php
		}
	}
   /*Notary form complete button end*/

	//$packageArray=array('Standard','Pro','Business');
	$notaryName="Customized Notary Stamp";
	$newNotaryName="Customized Notary Stamp -";
	if(isset($_POST['submit_cust_attr']))
	{
		$cust_inputName=$notaryName.' - '.$_POST["selec_color"].', '.$_POST["selec_shape"];
		$cust_insArray=array('order_item_name' => trim($cust_inputName),'order_item_type' => trim('line_item'),'order_id'  => trim($currn_id));
		$cust_lastid="";
		if(isset($_POST['spankage_name']) && get_post_meta($_POST['spankage_sid'],'dashboard_linked_store_product',true )=="no")
		{
			$updatedID=trim($currn_id);
			$getpost_id=$wpdb->get_results("SELECT `order_item_id` FROM `wp_woocommerce_order_items` WHERE `order_id`='$updatedID' AND `order_item_name`='{$notaryName}'");
			$cust_rowcount21 = $wpdb->num_rows;
			if($cust_rowcount21>0){
				$cust_lastid=$getpost_id[0]->order_item_id;
			}
			$wpdb->query($wpdb->prepare("UPDATE {$wpdb->base_prefix}woocommerce_order_items SET order_item_name='$cust_inputName' WHERE order_item_id='$cust_lastid'"));
		}else {
			$wpdb->insert("{$wpdb->base_prefix}woocommerce_order_items",$cust_insArray);
			$cust_lastid = $wpdb->insert_id;
		}

		$cust_ins2="INSERT INTO {$wpdb->base_prefix}woocommerce_order_itemmeta
		(order_item_id, meta_key, meta_value) VALUES
		('$cust_lastid', '_qty', '1' ),
		('$cust_lastid', 'pa_color', '".strtolower($_POST["selec_color"])."' ),
		('$cust_lastid', 'pa_shape', '".strtolower($_POST["selec_shape"])."' ),
		('$cust_lastid', 'commissioned-name', '".strtolower($_POST["notary_commission_name"])."' ),
		('$cust_lastid', 'notary-id', '".strtolower($_POST["notary_id_number"])."' ),
		('$cust_lastid', 'expiration-date', '".strtolower($_POST["notary_expiration_date"])."' )";
		$wpdb->query($cust_ins2);
		$author_obj = get_user_by('id', $userInfo);
		echo "<script> alert('You will receive your notary stamp within the next three to five business days. You will receive a USPS tracking number when your stamp ships.'); window.location.href=window.location.href;</script>";
	}


	/*===== Update Group Manager Information ======*/
	if(isset($_POST['submit_group_cust_attr']))
	{
		global $wpdb;
		if(isset($_POST['group_purId']))
		{
			$groupPurchaseID=trim($_POST['group_purId']);
			$insert_empty_array=array();
			$select_group_query=$wpdb->get_row("SELECT product_variations,order_id FROM {$wpdb->base_prefix}group_purchase WHERE id='$groupPurchaseID'");
			if(isset($select_group_query->product_variations) && $select_group_query->product_variations!="")
			{

				$get_data=json_decode($select_group_query->product_variations);
				$insert_empty_array=$get_data;

			}
			$variationinsarr[]=strtolower($_POST["selec_color"]);
			$variationinsarr[]=strtolower($_POST["selec_shape"]);
			$variationinsarr[]=strtolower($_POST["notary_commission_name"]);
			$variationinsarr[]=strtolower($_POST["notary_id_number"]);
			$variationinsarr[]=strtolower($_POST["notary_expiration_date"]);

			$select_itemgroup_id=$wpdb->get_row("SELECT `order_item_id`,`order_item_name` FROM `{$wpdb->base_prefix}woocommerce_order_items` WHERE `order_item_name`='Customized Notary Stamp' AND `order_id`=$select_group_query->order_id");

			if(isset($select_itemgroup_id->order_item_id) && $select_itemgroup_id->order_item_id!="")
			{
				$orderItemId=$select_itemgroup_id->order_item_id;
				$orderItemName=$select_itemgroup_id->order_item_name;
				$custg_inputName=$orderItemName.' - '.$_POST["selec_color"].', '.$_POST["selec_shape"];
				$wpdb->query($wpdb->prepare("UPDATE {$wpdb->base_prefix}woocommerce_order_items SET order_item_name='$custg_inputName' WHERE order_item_id='$orderItemId'"));

				$cust_insg2="INSERT INTO {$wpdb->base_prefix}woocommerce_order_itemmeta
				(order_item_id, meta_key, meta_value) VALUES
				('$orderItemId', 'pa_color', '".strtolower($_POST["selec_color"])."' ),
				('$orderItemId', 'pa_shape', '".strtolower($_POST["selec_shape"])."' ),
				('$orderItemId', 'commissioned-name', '".strtolower($_POST["notary_commission_name"])."' ),
				('$orderItemId', 'notary-id', '".strtolower($_POST["notary_id_number"])."' ),
				('$orderItemId', 'expiration-date', '".strtolower($_POST["notary_expiration_date"])."' )";
				$wpdb->query($cust_insg2);
			}
			else
			{
				$select_itemgroup_id=$wpdb->get_row("SELECT `order_item_id` FROM `{$wpdb->base_prefix}woocommerce_order_items` WHERE `order_item_name`LIKE('Customized Notary Stamp - %') AND `order_id`=$select_group_query->order_id");
				if(isset($select_itemgroup_id->order_item_id) && $select_itemgroup_id->order_item_id!="")
				{
					$orderItemId=$select_itemgroup_id->order_item_id;

					$scheck=$wpdb->get_row("SELECT `meta_value` FROM `{$wpdb->base_prefix}woocommerce_order_itemmeta` WHERE `meta_key`='_qty' AND `order_item_id`='$orderItemId'");
					if(isset($scheck->meta_value) && $scheck->meta_value!="")
					{
						$qty=(int)$scheck->meta_value;
						if($qty>1)
						{
							$qty=$qty-1;
							$wpdb->query($wpdb->prepare("UPDATE {$wpdb->base_prefix}woocommerce_order_itemmeta SET meta_value='$qty' WHERE `meta_key`='_qty' AND `order_item_id`='$orderItemId'"));
							$custg_inputName='Customized Notary Stamp - '.$_POST["selec_color"].', '.$_POST["selec_shape"];
							$cust_insArray=array('order_item_name' => trim($custg_inputName),'order_item_type' => trim('line_item'),'order_id'  => trim($select_group_query->order_id));
							$wpdb->insert("{$wpdb->base_prefix}woocommerce_order_items",$cust_insArray);
							$cust_lastid = $wpdb->insert_id;
							$cust_ins2="INSERT INTO {$wpdb->base_prefix}woocommerce_order_itemmeta
							(order_item_id, meta_key, meta_value) VALUES
							('$cust_lastid', '_qty', '1' ),
							('$cust_lastid', 'pa_color', '".strtolower($_POST["selec_color"])."' ),
							('$cust_lastid', 'pa_shape', '".strtolower($_POST["selec_shape"])."' ),
							('$cust_lastid', 'commissioned-name', '".strtolower($_POST["notary_commission_name"])."' ),
							('$cust_lastid', 'notary-id', '".strtolower($_POST["notary_id_number"])."' ),
							('$cust_lastid', 'expiration-date', '".strtolower($_POST["notary_expiration_date"])."' )";
							$wpdb->query($cust_ins2);
							echo "<script> alert('You will receive your notary stamp within the next three to five business days. You will receive a USPS tracking number when your stamp ships.'); window.location.href=window.location.href;</script>";

						}
					}

				}

			}
			$new_object= new stdClass();
			$progroup_id=$_POST['add_product'];
			$new_object->$progroup_id=$variationinsarr;
			$insert_empty_array[]=$new_object;
			$prodVariation=json_encode($insert_empty_array);
			/*echo $prodVariation;*/


			$wpdb->query($wpdb->prepare("UPDATE {$wpdb->base_prefix}group_purchase SET product_variations='$prodVariation' WHERE id='$groupPurchaseID'"));
		}
		$author_obj = get_user_by('id', $userInfo);
		echo "<script> alert('You will receive your notary stamp within the next three to five business days. You will receive a USPS tracking number when your stamp ships.'); window.location.href=window.location.href;</script>";
	}
	/* form creations */
	$sid=$spackage_name="";
	$check_group_member_query=$wpdb->get_results("SELECT * FROM `wp_group_purchase` WHERE `user_id`='$userInfo' AND `parent_id` !=0 ORDER BY `id` DESC LIMIT 1");
	$member_number_count=$wpdb->num_rows;
	$check_group_form_query=$wpdb->get_results("SELECT * FROM `wp_group_purchase` WHERE `order_id`='$currn_id'");
	$check_group_form_count=$wpdb->num_rows;
	$cust_query2_new="SELECT * FROM {$wpdb->base_prefix}woocommerce_order_items WHERE `order_item_name`='{$notaryName}' AND `order_id`='$currn_id'";

	$sresult=$wpdb->get_results($cust_query2_new);
	$cust_rowcount2 = $wpdb->num_rows;
	if($cust_rowcount2>0){
		$sid=$sresult[0]->order_item_id;
		$spackage_name=$sresult[0]->order_item_name;
	}
	$getstatus=$wpdb->get_results("SELECT `status` FROM `{$wpdb->base_prefix}notary_form` WHERE `user_id`= $userInfo AND `order_id`=$currn_id AND `status` IN (1,2)");
	$getstatus_count=$wpdb->num_rows;
	$getstatus1=$wpdb->get_results("SELECT `status` FROM `{$wpdb->base_prefix}notary_form` WHERE `user_id`= $userInfo AND `order_id`=$currn_id1 AND `status` IN (1,2)");

	$getstatus_count1=$wpdb->num_rows;
	if($getstatus_count>0 || $getstatus_count1>0)
	{
		if(((get_post_meta($package_id,'dashboard_linked_store_product',true )!="no") || (get_post_meta($package_id,'dashboard_linked_store_product',true)=="no" && $spackage_name==$notaryName) && $check_group_form_count < 1 )) {
			$cust_query2="SELECT * FROM {$wpdb->base_prefix}woocommerce_order_items WHERE `order_item_name` like 'Customized Notary Stamp -%' AND `order_id`='$currn_id'";
			$wpdb->get_results($cust_query2);
			$cust_rowcount = $wpdb->num_rows;
			if($cust_rowcount<1){
				$stampInfo=array('spankage_name'=>$cust_name,'spankage_sid'=>$sid);
				getStampInfoForm($stampInfo);
			}
		}elseif (get_post_meta($package_id,'dashboard_linked_store_product',true )=="no" && $check_group_form_count > 0) {
			$check_group_form_query=$wpdb->get_results("SELECT * FROM `wp_group_purchase` WHERE `order_id`='$currn_id'");
			if(!empty($wpdb->num_rows)){
				foreach($check_group_form_query as $groupMember){

					$product_variations=json_decode($groupMember->product_variations);
					$itest="notexist";
					foreach ($product_variations as $first_row) {
						foreach ($first_row as $key => $value) {

							if(get_post_meta($key,'product_variation_on_dash_board_single',true )=="yes")
							{
								$itest="exist";
							}

						}
					}
					if($itest=="notexist"){
						$group_purId=$groupMember->id;
						$group_planId=$groupMember->plan_id;
						$group_member_name=$groupMember->name;
						$add_product=json_decode($groupMember->add_product);

						if($groupMember->parent_id==0){
							$titleName="Group Manager Custom Notary Information";
						}else{
							$titleName="Member Custom Notary Information";
						}
						foreach ($add_product as $value_pro) {
							if(get_post_meta($value_pro,'product_variation_on_dash_board_single',true )=="yes")
							{
								$stampInfo=array('group_planId'=>$group_planId,'group_purId'=>$group_purId,'gTitleName'=>$titleName,'add_product'=>$value_pro,'group_member_name'=>$group_member_name);
								/* Memeber Notary Form Hide Condition. */
								if($titleName!="Member Custom Notary Information")
								getGroupStampInfo($stampInfo);
							}
						}
					}
				}
			}
		}
		elseif($member_number_count>0)
		{

			$member_id_order=$check_group_member_query[0]->id;
			$member_order_id=$check_group_member_query[0]->order_id;
			$check_group_form_query1=$wpdb->get_results("SELECT * FROM `wp_group_purchase` WHERE `order_id`='$member_order_id' AND id='$member_id_order' ");
			if(!empty($wpdb->num_rows)){
				foreach($check_group_form_query1 as $groupMember){

					$product_variations=json_decode($groupMember->product_variations);

					$itest="notexist";
					foreach ($product_variations as $first_row) {
						foreach ($first_row as $key => $value) {

							if(get_post_meta($key,'product_variation_on_dash_board_single',true )=="yes")
							{
								$itest="exist";
							}

						}
					}
					if($itest=="notexist"){
						$group_purId=$groupMember->id;
						$group_planId=$groupMember->plan_id;
						$group_member_name=$groupMember->name;
						$add_product=json_decode($groupMember->add_product);

						if($groupMember->parent_id==0){
							$titleName="Group Manager Custom Notary Information";
						}else{
							$titleName="Member Custom Notary Information";
						}
						foreach ($add_product as $value_pro) {
							if(get_post_meta($value_pro,'product_variation_on_dash_board_single',true )=="yes")
							{
								$stampInfo=array('group_planId'=>$group_planId,'group_purId'=>$group_purId,'gTitleName'=>$titleName,'add_product'=>$value_pro,'group_member_name'=>$group_member_name);
								/* Memeber Notary Form Hide Condition.
								if($titleName!="Member Custom Notary Information")*/
								getGroupStampInfo($stampInfo);
							}
						}
					}
				}
			}
		}
	}
/* form end */


?>
</div>
<!-- Notary Stamp form for singe purchase -->
<?php function getStampInfoForm($stampInfo=array()){ global $wpdb; ?>
	<div class="viewOrderForm row">
		<div class="col-sm-12">
			<h3>Get Your Notary Stamp <?php
			echo do_shortcode('[fusion_tooltip title="Complete this form once you have been commissioned as a notary. Click submit and we will get started on your stamp." class="" id="" placement="top" trigger="hover"]?[/fusion_tooltip]');

?>

			</h3>
		</div>
		<form method="post" id="notarystampform">
			<input type="hidden" name="spankage_name" value="<?php echo $stampInfo['spankage_name']; ?>">
			<input type="hidden" name="spankage_sid" value="<?php echo $stampInfo['spankage_sid']; ?>">
			<div class="clearfix"></div>
			<div class="form-group col-sm-4">
				<label>Notary Commission Name:</label>
				<input type="text" value="" class="form-control" id="notary_commission_name" name="notary_commission_name" />
			</div>
			<div class="form-group col-sm-4">
				<label>Notary ID Number:</label>
				<input class="form-control number_validation" value="" id="notary_id_number" name="notary_id_number" type="text" />
			</div>
			<div class="form-group col-sm-4">
				<label>Notary Expiration Date:</label>
				<input type="text" class="form-control notary_expiration_date" value="" id="notary_expiration_date" name="notary_expiration_date" />
			</div>
			<div class="clearfix"></div>
			<div class="form-group col-sm-6">
				<label>Select Color:</label>
				<?php
				$getColor=$wpdb->get_results("SELECT taxo.`term_id`,terms.`name`,terms.`slug` FROM {$wpdb->base_prefix}term_taxonomy as taxo INNER JOIN {$wpdb->base_prefix}terms as terms ON taxo.`term_id`=terms.`term_id` WHERE taxo.`taxonomy`='pa_color'");
				?>
				<select class="form-control" id="selec_color" name='selec_color'>
				<option value="">Choose an option</option>
				<?php
					foreach ($getColor as $corow) {
						echo "<option value='".$corow->name."'>".$corow->name."</option>";
					}
				?>
				</select>
			</div>
			<div class="form-group col-sm-6">
				<label>Select Shape:</label>
				<select class="form-control" id="selec_shape" name='selec_shape'>
					<option value="">Choose an option</option>
					<?php
					$getShape=$wpdb->get_results("SELECT taxo.`term_id`,terms.`name`,terms.`slug` FROM {$wpdb->base_prefix}term_taxonomy as taxo INNER JOIN {$wpdb->base_prefix}terms as terms ON taxo.`term_id`=terms.`term_id` WHERE taxo.`taxonomy`='pa_shape'");
					foreach ($getShape as $corow) {
						echo "<option value='".$corow->name."'>".$corow->name."</option>";
					}
					?>
				</select>
			</div>
			<div class="form-group col-sm-12">
				<input class="btn" type="submit" value="Submit" name='submit_cust_attr'>
			</div>
		</form>
	</div>
<?php } ?>

<!-- Notary Stamp form for group purchase -->
<?php function getGroupStampInfo($stampInfo=array()){ global $wpdb; ?>
	<div class="viewOrderForm row">
		<div class="col-sm-12">
			<h3><?php echo $stampInfo['gTitleName']; ?> (<?php echo $stampInfo['group_member_name']; ?>) <span class="questionMark" data-toggle="tooltip" title="Complete this form once you have been commissioned as a notary. Click submit and we will get started on your stamp.">?</span></h3>
		</div>
		<form method="post" id="notarystampform">
			<input type="hidden" name="group_planId" value="<?php echo $stampInfo['group_planId']; ?>">
			<input type="hidden" name="group_purId" value="<?php echo $stampInfo['group_purId']; ?>">
			<input type="hidden" name="add_product" value="<?php echo $stampInfo['add_product']; ?>">
			<div class="clearfix"></div>
			<div class="form-group col-sm-4">
				<label>Notary Commission Name:</label>
				<input type="text" value="" class="form-control" id="notary_commission_name" name="notary_commission_name" />
			</div>
			<div class="form-group col-sm-4">
				<label>Notary ID Number:</label>
				<input class="form-control number_validation" value="" id="notary_id_number" name="notary_id_number" type="text" />
			</div>
			<div class="form-group col-sm-4">
				<label>Notary Expiration Date:</label>
				<input type="text" class="form-control notary_expiration_date" value="" id="notary_expiration_date" name="notary_expiration_date" />
			</div>
			<div class="clearfix"></div>
			<div class="form-group col-sm-6">
				<label>Select Color:</label>
				<?php
				$getColor=$wpdb->get_results("SELECT taxo.`term_id`,terms.`name`,terms.`slug` FROM {$wpdb->base_prefix}term_taxonomy as taxo INNER JOIN {$wpdb->base_prefix}terms as terms ON taxo.`term_id`=terms.`term_id` WHERE taxo.`taxonomy`='pa_color'");
				?>
				<select class="form-control" id="selec_color" name='selec_color'>
				<option value="">Choose an option</option>
				<?php
					foreach ($getColor as $corow) {
						echo "<option value='".$corow->name."'>".$corow->name."</option>";
					}
				?>
				</select>
			</div>
			<div class="form-group col-sm-6">
				<label>Select Shape:</label>
				<select class="form-control" id="selec_shape" name='selec_shape'>
					<option value="">Choose an option</option>
					<?php
					$getShape=$wpdb->get_results("SELECT taxo.`term_id`,terms.`name`,terms.`slug` FROM {$wpdb->base_prefix}term_taxonomy as taxo INNER JOIN {$wpdb->base_prefix}terms as terms ON taxo.`term_id`=terms.`term_id` WHERE taxo.`taxonomy`='pa_shape'");
					foreach ($getShape as $corow) {
						echo "<option value='".$corow->name."'>".$corow->name."</option>";
					}
					?>
				</select>
			</div>
			<div class="form-group col-sm-12">
				<input class="btn" type="submit" value="Submit" name='submit_group_cust_attr'>
			</div>
		</form>
	</div>
<?php } ?>
<?php if($getincompletebutton==0)
{
	$usernewid=get_current_user_id();
	$getincID=$wpdb->get_row("SELECT * FROM `{$wpdb->base_prefix}notary_form` WHERE `user_id`='$usernewid' ORDER BY `notary_id` DESC LIMIT 1");
	if(isset($getincID->status) && $getincID->status==0)
	{
		$nt_id = base64_encode($getincID->notary_id);
	    $od_id = base64_encode($getincID->order_id);

	    $ln = get_site_url().'/notary-application-form/?n_id='. $nt_id.'&o_id='.$od_id;
	    $ln_gr = get_site_url().'/group-notary-application-form/?notary_id='.$nt_id;
		if($getincID->group_status ==1){
			 echo '<div><a href= "'.$ln_gr.'" class="btn completeBtn">Complete pending application</a></div>';
		}else{
			echo '<div><a href="'.$ln.'" class="btn completeBtn">Complete pending application</a></div>';
		}
	}
}
?>
<?php /*
<link href="<?php echo get_stylesheet_directory_uri(); ?>/jquery-ui.css" rel="stylesheet">
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/validation_js/ssn_validation.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/validation_js/numeric-1.2.6.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/validation_js/bezier.js"></script>
<script>
	jQuery(".notary_expiration_date").inputmask('99-99-9999');
	jQuery(function() {
		jQuery( ".notary_expiration_date" ).datepicker({
			dateFormat : 'mm-dd-yy',
			changeMonth : true,
			changeYear : true,
			minDate: 0
		});
	});
</script>
<script>
jQuery(document).ready(function(){
	jQuery(".number_validation").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl/cmd+A
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: Ctrl/cmd+C
			(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: Ctrl/cmd+X
			(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	jQuery("#notarystampform").submit(function(){
		console.log(jQuery("#notary_id_number").val().length);
		jQuery(".nformError").remove();
		var check=0;
		if(jQuery("#notary_commission_name").val()=="")
		{
			jQuery("#notary_commission_name").after("<span class='error nformError'>Please enater commission name.</span>");
			check=1;
		}
		if(jQuery("#notary_id_number").val()=="")
		{
			jQuery("#notary_id_number").after("<span class='error nformError'>Please enater Notary ID.</span>");
			check=1;
		}
		if(jQuery("#notary_id_number").val().length!=9 && jQuery("#notary_id_number").val()!="")
		{
			jQuery("#notary_id_number").after("<span class='error nformError'>Notary ID number shuld be 9 digit.</span>");
			check=1;
		}
		if(jQuery("#notary_expiration_date").val()=="")
		{
			jQuery("#notary_expiration_date").after("<span class='error nformError'>Please enater expiration date.</span>");
			check=1;
		}
		if(jQuery("#selec_color").val()=="")
		{
			jQuery("#selec_color").after("<span class='error nformError'>Please slect Notary color.</span>");
			check=1;
		}
		if(jQuery("#selec_shape").val()=="")
		{
			jQuery("#selec_shape").after("<span class='error nformError'>Please Notary shape.</span>");
			check=1;
		}
		if(check==1)
		{
			return false;
		}
	});
});
</script>*/ ?>
