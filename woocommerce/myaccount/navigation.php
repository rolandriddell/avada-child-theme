<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
?>
<div class="dashboardMenu clearfix">Dashboard Menu <i class="fa fa-bars"></i></div>
<nav class="woocommerce-MyAccount-navigation">
	<ul>
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
			 <?php
			 $userInfo=get_current_user_id();
			 global $wpdb;
			 if(esc_html( $label )=="Group Order")
					 {
					 	$subresults = $wpdb->get_results( "SELECT * FROM `wp_group_purchase` WHERE `user_id`='$userInfo' AND `parent_id`='0'");
					 	if($wpdb->num_rows > 0){?>
					 		<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
					 	<?php }
					 }
					 	
				$results = $wpdb->get_results( "SELECT * FROM `wp_notary_form` WHERE `user_id`='$userInfo'");
				if($wpdb->num_rows > 0 && esc_html( $label )!="Group Order"){ ?>
				<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
			</li>  
				<?php }else{
			 if(esc_html( $label )!="Notary Application Status" && esc_html( $label )!="Renew Commission" && esc_html( $label )!="Group Order") {
			 	?>
			<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
			</li>
			 <?php } } ?>
		<?php endforeach; ?>
		<?php if(isset($_COOKIE['gpackagesbtn'])) {
$cookie_value=$_COOKIE["gpackagesbtn"];?>
		<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
			<a href="<?php echo get_site_url();?>/group-purchase/?id=<?php echo $cookie_value; ?>">Incomplete Group Order</a>
		</li>

		<?php } ?>
		
		
	</ul>
</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
<script>
	jQuery(".dashboardMenu").click(function(){
		jQuery(".woocommerce-MyAccount-navigation").slideToggle();
	});
</script>
