<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.1
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product ) {
	return;
}

$attribute_keys = array_keys( $attributes );

do_action( 'woocommerce_before_add_to_cart_form' ); ?>
<form class="variations_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->get_id() ); ?>" data-product_variations="<?php echo htmlspecialchars( wp_json_encode( $available_variations ) ); // WPCS: XSS ok. ?>">
	<?php do_action( 'woocommerce_before_variations_form' ); ?>
	<?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
		<p class="stock out-of-stock"><?php esc_html_e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>
	<?php else : ?>

		<table class="variations" cellspacing="0">
			<tbody>
				<?php /*if(absint( $product->get_id()==735)){ ?>
					<tr>
						<td class="label"><label for="attribute_notary_commission_name">Notary Commission Name</label></td>
						<td class="value"><input type="text" id="attribute_notary_commission_name" class="form-control" required name="attribute_notary_commission_name"></td>
					</tr>
					<tr>
						<td class="label"><label for="attribute_notary_id_number">Notary ID Number</label></td>
						<td class="value"><input type="text" id="attribute_notary_id_number" class="number_validation" required name="attribute_notary_id_number" type="text" minlength="9" maxlength="9"></td>
					</tr>
					<tr>
						<td class="label"><label for="attribute_notary_expiration_date">Notary Expiration Date</label></td>
						<td class="value"><input type="text" class="notary_expiration_date" id="attribute_notary_expiration_date" required name="attribute_notary_expiration_date"></td>
					</tr>
				<?php }*/ ?>
				<?php foreach ( $attributes as $attribute_name => $options ) : ?>
					<tr>
						<td class="label"><label for="<?php echo esc_attr( sanitize_title( $attribute_name ) ); ?>"><?php echo wc_attribute_label( $attribute_name ); // WPCS: XSS ok. ?></label></td>
						<td class="value">
							<?php
								wc_dropdown_variation_attribute_options( array(
									'options'   => $options,
									'attribute' => $attribute_name,
									'product'   => $product,
								) );
							?>
						</td>
					</tr>
				<?php endforeach; ?>

				<?php // ThemeFusion edit for Avada theme: move the price reset button. ?>
				  <tr>
					<td class="label"></td>
					<td class="value">
						<div class="single_variation_price_reset">
							<div class="single_variation_wrap">
								<div class="avada-variation single_variation"></div>
							</div>

							<?php echo end( $attribute_keys ) === $attribute_name ? wp_kses_post( apply_filters( 'woocommerce_reset_variations_link', '<a class="reset_variations" href="#">' . esc_html__( 'Clear selection', 'Avada' ) . '</a>' ) ) : ''; ?>
						</div>
					</td>
				</tr>
			</tbody>
		</table>

		<div class="single_variation_wrap">
			<?php
				/**
				 * Hook: woocommerce_before_single_variation.
				 */
				do_action( 'woocommerce_before_single_variation' );

				/**
				 * Hook: woocommerce_single_variation. Used to output the cart button and placeholder for variation data.
				 *
				 * @since 2.4.0
				 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
				 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
				 */
				do_action( 'woocommerce_single_variation' );

				/**
				 * Hook: woocommerce_after_single_variation.
				 */
				do_action( 'woocommerce_after_single_variation' );
			?>
		</div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_after_variations_form' ); ?>
</form>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/jquery-ui.css" rel="stylesheet">
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/validation_js/ssn_validation.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/validation_js/numeric-1.2.6.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/validation_js/bezier.js"></script>
<script>

/*jQuery(document).ready(function(){
	jQuery(".number_validation").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
			// Allow: Ctrl/cmd+A
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: Ctrl/cmd+C
			(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: Ctrl/cmd+X
			(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
});
	jQuery(".notary_expiration_date").inputmask('99-99-9999');
	jQuery(function() {
		jQuery( ".notary_expiration_date" ).datepicker({
			dateFormat : 'mm-dd-yy',
			changeMonth : true,
			changeYear : true,
			minDate: 0
		});
	});*/
</script>

<?php do_action( 'woocommerce_after_add_to_cart_form' );

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
