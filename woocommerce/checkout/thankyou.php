<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
//Check back button issue
if(isset($_SESSION['redirecthomecount']))
{
	//echo $_SESSION['redirecthomecount'];
	$redirecturl=get_site_url(null,'/dashboard/' );
	//echo $redirecturl;
	header("Location: ".$redirecturl);

	exit;
}
	global $wpdb;
	$table_name = $wpdb->prefix . "group_tempuser";
	$table = $wpdb->prefix . "group_purchase";
	$expire = $wpdb->prefix . "expire_plan";
	$user_table  = $wpdb->prefix . "users";
	$renew = $wpdb->prefix . "group_renew";
	$end = date('Y-m-d h:i:s', strtotime('+4 years'));
	$st = date('Y-m-d h:i:s');
	$ip = $_SERVER['REMOTE_ADDR'];
	$p_id = (int)$order->user_id;
	/*==getting data if group-purchase==*/
     if(!empty ($_SESSION)) {
 		if(isset($_SESSION['parent']))
 		{
 			$parent = $_SESSION['parent'];
 			$_SESSION['redirecthomecount']=1;
 		}

	}
	   if(!empty($parent)){
	   $query1 ='SELECT * FROM '.$table_name.' WHERE id = '.$parent;
	   $manager_detail = $wpdb->get_results($query1);
	   $group_notary_id="";
	   $query2 ='SELECT * FROM '.$table_name.' WHERE parent_id = '.$parent;
       $member_detail = $wpdb->get_results($query2);
	   }
	   $qr2= 'SELECT max(`group_id`) as `group_val` FROM '.$expire.' WHERE `group_id` is NOT NULL';
	   $last_group_val = $wpdb->get_results($qr2);
	   $last_gval = $last_group_val[0]->group_val;
	   $new_group_id = $last_gval +1;
	   /*==getting data if group renewal process==*/
	   if(!empty ($_SESSION)) {
		    $renew_id = $_SESSION['renew_id'];
	    }
	   if(!empty($renew_id)){
	   $query1 ='SELECT * FROM '.$renew.' WHERE id = '.$renew_id;
	   $renew_detail = $wpdb->get_results($query1);
	   $group_id = $renew_detail[0]->group_id;
       $user_id = $renew_detail[0]->user_id;
       $purchase_id = $renew_detail[0]->purchase_id;
       $query2 ='SELECT * FROM '.$table.' WHERE `user_id` = '.$user_id.' AND `group_id`='.$group_id .' AND `id`='.$purchase_id .' AND `trash`=0';
       $old_order = $wpdb->get_results($query2);
	   }


	  // echo "<pre>";print_r($order);
	   ?>
	<div class="woocommerce-order">
		<?php if ( $order ) : ?>
			<?php if ( $order->has_status( 'failed' ) ) : ?>
				<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>
				<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
					<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
					<?php if ( is_user_logged_in() ) : ?>
						<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
					<?php endif; ?>
				</p>
			<?php else : ?>
			<div class="tnsThankyou">
				<div id="abc"></div>
			<?php
				$flag = 0;
				$plan_ids = 0;
				$items_o = wc_get_order($order->get_order_number());
				$order_items = $items_o->get_items();
				foreach($order_items as $item => $values) {
					if( 'plan' == get_post_meta($values['product_id'], "wpcf-product_type", true)){
						$flag =1;
						$plan_ids = $values['product_id'];
					}
				}
			?>
			<?php if($flag == 1) { ?>
				<?php
				$current_user = wp_get_current_user();
				if(!empty($manager_detail) || !empty($old_order)) {
					if(!empty($manager_detail[0]->add_product)){
						$p_id = (int)$order->user_id;
	 					$o_id = (int)$order->get_id();
	 					$order_data = $order->get_data();
						$sql_ajx = "insert into ".$wpdb->prefix."notary_form set
							first_name = '".$manager_detail[0]->name."',
							middle_name = '".$manager_detail[0]->middle_name."',
							last_name = '".$manager_detail[0]->last_name."',
							email_address = '".$manager_detail[0]->email."',
							mailing_address = '".$order_data['billing']['address_1']."',
							city = '".$order_data['billing']['city']."',
							zipcode = '".$order_data['billing']['postcode']."',
							order_id = '".$o_id."',
							user_id = '".$p_id."',
							status = 0, group_status = 1";
						$result_ajx = $wpdb->query($sql_ajx);
						$group_notary_id=$wpdb->insert_id;
					?>

						<div class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( '<h1>Thank You.</h1><br><p><strong>Thank you for your order.</strong></p><h3>What happens next?</h3> <p> Members of your group have been sent emails with login information. You can monitor member progress in the <a style="color: #747474" href="'.get_site_url().'/dashboard/">Dashboard</a>.</p><h3>File your application.</h3><p>Click the button below to start and file the state\'s application. That will take about 5 minutes. you may also complete the application later by signing into your account and visiting the dashboard.</p>', 'woocommerce' ), $order ); ?></div>

						<p class="tnsLink"><a href='<?php echo get_site_url(); ?>/group-notary-application-form/?notary_id=<?php echo base64_encode($group_notary_id); ?>'>Complete The Application</a></p>

						<p class="tnsLink"><input class="btn" type="button" id="save_download_form1"  value="Save For Later"></p>
						<input type="hidden" class="action" value="group_form_add">
						<?php /*<p style="text-align:center;"><small>or sign-in to your account to complete whenever you are ready.</small></p>
						<p class="simpleText">An email invitation has been sent to all member(s). You can check their status on the <a style="color: #747474" href="<?php echo get_site_url();?>/dashboard/">dashboard</a> page.</p>*/ ?>

				<?php }else{   ?>
						<h1>Thank You for Your Order</h1>
						<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">Emails have been sent to the group members that you created in this order. The receipt for this purchase was delivered to your email address. Visit the <a style="color: #747474" href="<?php echo get_site_url();?>/dashboard/">Dashboard</a> to monitor your people's progress on the state's application, reset passwords, and resend the invite email.</p>
						<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">If you have questions, please do not hesitate to contact us!</p>
				<?php } ?>

			  <?php }else { ?>
					<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( '<h1>Thank You.</h1><br><p>Thank you for your order. Click the button below to start and file the state\'s application. This will take about 5 minutes. You may also complete the application later by signing into your account and visiting the dashboard.</p>', 'woocommerce' ), $order ); ?></p>
					<p class="tnsLink"><a href='<?php echo get_site_url(); ?>/notary-application-form?o_id=<?php echo base64_encode($order->get_order_number());?>'>Complete The Application</a></p>
					<p class="tnsLink"><input class="btn" type="button" id="save_download_form1"  value="Save For Later"></p>
					<input type="hidden" class="action" value="r_pdf">
					<!--<p style="text-align:center;"><small>or sign-in to your account to complete whenever you are ready.</small></p>-->
			<?php }?>

			<?php }else{?>
					<h1>Thank you for your order!</h1>
					<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">
						The receipt for this order has been sent to your email account. You will receive a tracking number when the item is sent. Notary training is available through the <a href="<?php echo get_site_url();?>/dashboard/">Dashboard</a>.
					</p>
					<div class="login-btn thank-you">
             <?php if ( is_user_logged_in() ) { ?>
                <a class="myAccount" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Dashboard','woothemes'); ?>"><?php _e('Dashboard','woothemes'); ?></a>

                <a class="logoutBtn" href="<?php echo wp_logout_url(get_permalink());  ?>" title="<?php _e('Logout','woothemes'); ?>"><?php _e('Logout','woothemes'); ?></a>
             <?php }
             else { ?>
                <a class="login" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Login / Register','woothemes'); ?>"><?php _e('Login','woothemes'); ?></a>
            <?php } ?>
          </div>
					<?php /*<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received">Thank you for your order. you can check the status of your item in your <a style="color: #747474" href="<?php echo get_site_url();?>/dashboard/">dashboard</a>. If you ordered training for another, an email invitation has been sent to the email address that you provided.</p>*/ ?>
			<?php } ?>
			<div clas="clear"></div>
			</div>
			<?php /*
			<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
			<li class="woocommerce-order-overview__order order">
			<?php _e( 'Order number:', 'woocommerce' ); ?>
			<strong><?php echo $order->get_order_number(); ?></strong>
			</li>
			<li class="woocommerce-order-overview__date date">
			<?php _e( 'Date:', 'woocommerce' ); ?>
			<strong><?php echo wc_format_datetime( $order->get_date_created() ); ?></strong>
			</li>
			<li class="woocommerce-order-overview__total total">
			<?php _e( 'Total:', 'woocommerce' ); ?>
			<strong><?php echo $order->get_formatted_order_total(); ?></strong>
			</li>
			<?php if ( $order->get_payment_method_title() ) : ?>
			<li class="woocommerce-order-overview__payment-method method">
			<?php _e( 'Payment method:', 'woocommerce' ); ?>
			<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
			</li>
			<?php endif; ?>
			</ul>
			<?php */?>
			<!--==CUSTOM CODE FOR DIFFERENT FUNCTIONALITY==-->
			<?php if(!empty($manager_detail) && !empty($member_detail) && isset($order) && (!$order->has_status( 'failed' ))){
					$plan = wc_get_product($manager_detail[0]->plan_id);
					 $end = date('Y-m-d h:i:s', strtotime('+4 years'));
				     $st = date('Y-m-d h:i:s');
					/*==SAVE DATA IN NEW TABLE AS FINAL COMPLETE PURCHASE OF GROUP MANAGER==*/
					 $new_list = date('Y-m-d h:i:s');
					 $ip = $_SERVER['REMOTE_ADDR'];
					 $p_id = (int)$order->user_id;
					 $o_id = (int)$order->get_id();
					 $val1 = $manager_detail[0]->plan_id;
					 $val2 = $manager_detail[0]->name;
					 $val21 = $manager_detail[0]->middle_name;
					 $val22 = $manager_detail[0]->last_name;
					 $val3 = $manager_detail[0]->email;
					 $val4 = $manager_detail[0]->add_product;
					 $val5 = $manager_detail[0]->product_variations;
					 $data2 = array('plan_id'=>$val1,'parent_id'=>0,'reffer_id'=>$p_id,'order_id'=>$o_id,'name'=>$val2,'middle_name'=>$val21,'last_name'=>$val22,'email'=>$val3,'add_product'=>$val4,'product_variations'=>$val5,'ip_address'=>$ip,'created_at'=>$new_list);
					 $data_type = array('%d','%d','%d','%d','%s','%s','%s','%s','%s','%s','%s','%s');
				     $wpdb->insert($table, $data2,$data_type );
					 $last_user =  $wpdb->insert_id;
					 /*==UPDATE EXISTING TABLE BY WORDPRESS USER ID AND GROUP ID OF GROUP MANAGER==*/
					 $wpdb->update($table, array('user_id' => $p_id,'group_id'=>$new_group_id),array('ID' => $last_user ),array('%d','%d'),array('%d'));
					/*$sql_ajx = "insert into `wp_notary_form` set
					first_name = '".$val2."',
					middle_name = '".$val21."',
					last_name = '".$val22."',
					email_address = '".$val3."',
					order_id = '".$o_id."',
					user_id = '".$p_id."',
					status = 0, group_status = 1";
					$result_ajx = $wpdb->query($sql_ajx);
					$notary_id=base64_encode($wpdb->insert_id);*/
					 /*==SET EXPIRE DATE FOR PLAN PURCHASE OF GROUP MANAGER==*/
					 $expire_data = array('plan_id'=>$val1,'user_id'=>$p_id,'order_id'=>$o_id,'reffer_id'=>$p_id,'group_id'=>$new_group_id,'order_date'=>$st,'expire_date'=>$end,'ip_address'=>$ip);
					 $expire_data_type = array('%d','%d','%d','%d','%d','%s','%s','%s');
					 $wpdb->insert($expire, $expire_data,$expire_data_type);

					 foreach($member_detail as $member_user){
					 /*==SAVE DATA IN NEW TABLE AS FINAL COMPLETE PURCHASE OF GROUP MEMBER==*/
					 $data = array('plan_id'=>$manager_detail[0]->plan_id,'parent_id'=>$last_user,'reffer_id'=>$p_id,'order_id'=>$o_id,'name'=>$member_user->name,'middle_name'=>$member_user->middle_name,'last_name'=>$member_user->last_name,'email'=>$member_user->email,'add_product'=>$member_user->add_product,'product_variations'=>$member_user->product_variations,'ip_address'=>$ip,'created_at'=>$new_list);
					 $data_type = array('%d','%d','%d','%d','%s','%s','%s','%s','%s','%s','%s','%s');
				     $wpdb->insert($table, $data,$data_type );
				     $now_user =  $wpdb->insert_id;
				     $password = rand();
				     /*==CREATE USER PROGRAMATICALLY==*/
				     $already_user = get_user_by('email', $member_user->email);
				     if(!empty($already_user->ID)){
						$member_id = $already_user->ID ;
					 }else{
						$member_id = wc_create_new_customer($member_user->email, $username ="", $password);
					 }
				     /*==UPDATE EXISTING TABLE BY WORDPRESS USER ID AND GROUP ID OF GROUP MEMBER==*/
				     $wpdb->update($table, array('user_id' => $member_id,'group_id' => $new_group_id),array('ID' => $now_user ),array('%d','%d'),array('%d'));
				     /*==SET EXPIRE DATE FOR PLAN PURCHASE OF GROUP MEMBER==*/
					 $expire_data = array('plan_id'=>$manager_detail[0]->plan_id,'user_id'=>$member_id,'order_id'=>$o_id,'reffer_id'=>$p_id,'group_id'=>$new_group_id,'order_date'=>$st,'expire_date'=>$end,'ip_address'=>$ip);
					 $expire_data_type = array('%d','%d','%d','%d','%d','%s','%s','%s');
					 $wpdb->insert($expire,$expire_data,$expire_data_type);
				     $user_info = get_userdata($member_id);
				     /*==EMAIL  MEMBER FOR COMPLETE NOTARY APPLICATION FORM==*/
				     $order_data = $order->get_data();
				     $sql_ajx_member = "insert into ".$wpdb->prefix."notary_form set
											first_name = '".$member_user->name."',
											middle_name = '".$member_user->middle_name."',
											last_name = '".$member_user->last_name."',
											email_address = '".$member_user->email."',
											mailing_address = '".$order_data['billing']['address_1']."',
											city = '".$order_data['billing']['city']."',
											zipcode = '".$order_data['billing']['postcode']."',
											order_id = '".$o_id."',
											user_id = '".$member_id."',
											status = 0, group_status = 1";
										$result_ajx_member = $wpdb->query($sql_ajx_member);
										$member_notary_id=$wpdb->insert_id;
				     if(empty($already_user)) {
					$acc = '<!DOCTYPE html> <html> <head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Emailer</title> <style> #outlook a{padding:0;} body{width:100% !important; background-color:#fff;-webkit-text-size-adjust:none; -ms-text-size-adjust:none;margin:0 !important; padding:0 !important;}  .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} ol li {margin-bottom:15px;} img{height:auto; line-height:100%; outline:none; text-decoration:none;} #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;} p {margin: 1em 0;} h1, h2, h3, h4, h5, h6 {color:#222222 !important; font-family:Arial, Helvetica, sans-serif; line-height: 100% !important;} table td {border-collapse:collapse;} .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;} .im {color:black;} div[id="tablewrap"] { width:100%; max-width:600px!important; } table[class="fulltable"], td[class="fulltd"] { max-width:100% !important; width:100% !important; height:auto !important; } @media screen and (max-device-width: 430px), screen and (max-width: 430px) { td[class=emailcolsplit]{ width:100%!important; float:left!important; padding-left:0!important; max-width:430px !important; } td[class=emailcolsplit] img { margin-bottom:20px !important; } } </style> </head> <body><table id="backgroundTable" width="100%" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:14px;line-height:1.4;background:#fff;height:auto !important; margin:0; padding:0; width:100% !important;"> <tr> <td align="center" style="padding-bottom:20px;"> <a href="'.get_site_url().'"> <img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /> </a> </td> </tr> <tr> <td align="center" valign="top"> <div id="tablewrap" style="width:100% !important; max-width:600px !important;margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important;"> <table id="contenttable" width="600" align="center" cellpadding="20" cellspacing="0" border="0" style="background-color:#f7f7f7; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:600px !important;-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);"> <tr> <td width="100%" style="padding: 0 15px;"> <table border="0" cellspacing="0" cellpadding="10" width="100%"> <tr> <td width="100%" align="left" colspan="2" style="padding: 25px 15px 0px;"> <h4 style="font-size:21px; margin:0 0 15px !important;text-transform:capitalize;">Hi '.$member_user->name.',</h4> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">You have been signed up by '.$val2.' to become a notary public. The next step is to complete the application. This will take about ten minutes. To get started, please login to the <a href="https://texasnotarysolutions.com/dashboard">website</a> on your computer of mobile device using the information below.</p> </td> </tr> <tr> <td> <table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee;"> <tr> <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 10px 5px; font-weight:normal;"><strong>Email:  </strong><span style="text-decoration:none;color:#000;">'.$member_user->email.'</span></td></tr><tr><td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:5px 10px 10px; font-weight:normal;"><strong>Password:  </strong>'.$password.'</td></tr> </table> </td> </tr> </table> </td> </tr> <tr> <td width="100%" valign="top" style="padding: 10px 25px 20px;"> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0;  padding:0; font-weight:normal;">Please follow the link below to complete the notary application form.<br><a style="margin-top:10px;display: inline-block !important;text-decoration: none;text-align: center;padding: 10px !important;color: #fff;background: #334667;" href="'.get_site_url().'/group-notary-application-form/?notary_id='. base64_encode($member_notary_id).'">Complete The Notary Application</a></p> <div class="thanks-note"> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0;  padding:0; font-weight:normal;">If you have any questions, please do not hesitate to <a href="'.get_site_url().'/contact-us/">contact us</a>.</p></div> </td> </tr> </table> </div></td> </tr> <tr> <td height="40" width="100%" align="center" valign="top" style="padding:20px 0; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0;font-weight:normal;"> <a href="'.get_site_url().'">Texas Notary Solutions</a> </td> </tr> </table></body></html>';
				      $for  = "You're Becoming a Notary";
					  $header = "From:Texas Notary Solutions <info@texasnotarysolutions.com> \r\n";
				      $header .= "MIME-Version: 1.0\r\n";
					  $header .= "Content-type: text/html\r\n";
					  wp_mail( $member_user->email, $for, $acc, $header);

					  //mail($member_user->email,$for,$acc,$header);
					  //echo $acc;
				     }else {
					 if($already_user){
					$acc = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Emailer</title> <style> #outlook a{padding:0;} body{width:100% !important; background-color:#fff;-webkit-text-size-adjust:none; -ms-text-size-adjust:none;margin:0 !important; padding:0 !important;}  .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} ol li {margin-bottom:15px;} img{height:auto; line-height:100%; outline:none; text-decoration:none;} #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;} p {margin: 1em 0;} h1, h2, h3, h4, h5, h6 {color:#222222 !important; font-family:Arial, Helvetica, sans-serif; line-height: 100% !important;} table td {border-collapse:collapse;} .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;} .im {color:black;} div[id="tablewrap"] { width:100%; max-width:600px!important; } table[class="fulltable"], td[class="fulltd"] { max-width:100% !important; width:100% !important; height:auto !important; } @media screen and (max-device-width: 430px), screen and (max-width: 430px) { td[class=emailcolsplit]{ width:100%!important; float:left!important; padding-left:0!important; max-width:430px !important; } td[class=emailcolsplit] img { margin-bottom:20px !important; } } </style> </head> <body><table id="backgroundTable" width="100%" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:14px;line-height:1.4;background:#fff;height:auto !important; margin:0; padding:0; width:100% !important;"> <tr> <td align="center" style="padding-bottom:20px;"> <a href="'.get_site_url().'"> <img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /> </a> </td> </tr> <tr> <td align="center" valign="top"> <div id="tablewrap" style="width:100% !important; max-width:600px !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important;"> <table id="contenttable" width="600" align="center" cellpadding="20" cellspacing="0" border="0" style="background-color:#f7f7f7; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:600px !important;-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);"> <tr> <td width="100%"> <table width="100%" border="0" cellspacing="0" cellpadding="10"> <tr> <td width="100%" align="left" colspan="2"> <h4 style="font-size:21px;margin:0 !important;">Hi '.$member_user->name.',</h4> </td> </tr> <tr> <td width="100%" valign="top" colspan="2"> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">Please follow the link below to complete the notary application form.<br><a style="margin-top:10px;display: inline-block;text-decoration: none;padding: 10px;color: #fff;background: #334667;" href="'.get_site_url().'/group-notary-application-form/?notary_id='. base64_encode($member_notary_id).'">Complete Notary Application</a></p> <div class="thanks-note"> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">If you have any questions, please do not hesitate to <a href="'.get_site_url().'/contact-us/">contact us</a>.</p></td> </tr> </table> </td> </tr> <tr> <td height="40" align="center" valign="top" style="padding:20px 0;font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;"> <a href="'.get_site_url().'">Texas Notary Solutions</a> </td> </tr> </table> </div> </td> </tr></table></body></html>';
				      $for  = "Complete The Notary Application Form";
					  $header = "From:Texas Notary Solutions <info@texasnotarysolutions.com> \r\n";
				      $header .= "MIME-Version: 1.0\r\n";
					  $header .= "Content-type: text/html\r\n";
					 wp_mail($member_user->email,$for,$acc,$header);
					  } }
					/*==EMAIL TO ALL MEMBER FOR NOTIFY EXPIRY DATE OF PLAN(commented this)==*/
				     $acc2 = '<!DOCTYPE html> <html> <head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Emailer</title> <style> #outlook a{padding:0;} body{width:100% !important; background-color:#fff;-webkit-text-size-adjust:none; -ms-text-size-adjust:none;margin:0 !important; padding:0 !important;}  .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} ol li {margin-bottom:15px;} img{height:auto; line-height:100%; outline:none; text-decoration:none;} #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;} p {margin: 1em 0;} h1, h2, h3, h4, h5, h6 {color:#222222 !important; font-family:Arial, Helvetica, sans-serif; line-height: 100% !important;} table td {border-collapse:collapse;} .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;} .im {color:black;} div[id="tablewrap"] { width:100%; max-width:600px!important; } table[class="fulltable"], td[class="fulltd"] { max-width:100% !important; width:100% !important; height:auto !important; } @media screen and (max-device-width: 430px), screen and (max-width: 430px) { td[class=emailcolsplit]{ width:100%!important; float:left!important; padding-left:0!important; max-width:430px !important; } td[class=emailcolsplit] img { margin-bottom:20px !important; } } </style> </head> <body><table id="backgroundTable" width="100%" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:14px;line-height:1.4;background:#fff;height:auto !important; margin:0; padding:0; width:100% !important;"><tr> <td align="center" style="padding-bottom:20px;"> <a href="'.get_site_url().'"> <img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /> </a> </td> </tr> <tr> <td align="center" valign="top"> <div id="tablewrap" style="width:100% !important; max-width:600px !important;  margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important;"> <table id="contenttable" width="600" align="center" cellpadding="20" cellspacing="0" border="0" style="background-color:#f7f7f7; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:600px !important;-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);"> <tr> <td width="100%"> <table width="100%" border="0" cellspacing="0" cellpadding="10"> <tr> <td width="100%" align="left" colspan="2"> <h4 style="font-size:21px; margin:0 !important;">Hi '.$member_user->name.',</h4> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">I am pleased to inform that you have purchased <strong> '.$plan->name.' </strong> package. Please find your plan expire details as follows :</p> </td> </tr> <tr> <td width="100%" align="left" colspan="2"> <table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee;"> <tr> <td width="50%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;"><strong>Plan Name:  </strong>'.$plan->name.'</td> <td width="50%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;"><strong>Plan Purchase Date:  </strong>'.date("F d,Y", strtotime($st)).'</td> </tr> </table> </td> </tr> <tr> <td width="100%" align="left" colspan="2"> <table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee;"> <tr> <td width="50%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;"><strong>Plan Start Date:  </strong>'.date("F d,Y", strtotime($st)).'</td> <td width="50%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;"><strong>Plan Expiry Date:  </strong>'.date("F d,Y", strtotime($end)).'</td></tr></table></td></tr></table></td> </tr> <tr> <td width="100%" valign="top bottom" style="padding-left:20px; padding-right:20px;"> <div class="thanks-note" style="width:100% !important; margin-top:0 !important; margin-right: 0 !important; margin-bottom:0 !important; margin-left: 0 !important;"> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0;  padding:0; font-weight:normal;">If you have any questions, please do not hesitate to <a href="'.get_site_url().'/contact-us/">contact us</a>.</p> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0;  padding:0; font-weight:normal;">We&#39;re here to help! Thank you for your business.</p> </div> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0;  padding:0; font-weight:normal;">Andrew Whitfield<br>Founder</p> </td> </tr> </table> </div> </td> </tr> <tr> <td height="40" align="center" valign="top" style="padding:20px 0;font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0;  padding:0; font-weight:normal;"> <a href="'.get_site_url().'">Texas Notary Solutions</a> </td> </tr> </table></body></html>';
					 $for2  = "Your package will be expire on ".date('F d,Y', strtotime($end));
					 $header2 = "From:Texas Notary Solutions <info@texasnotarysolutions.com> \r\n";
				     $header2 .= "MIME-Version: 1.0\r\n";
					 $header2 .= "Content-type: text/html\r\n";
					 //mail($member_user->email,$for2,$acc2,$header2);
					 } }else{

						if($flag == 1 && empty($_SESSION['renew-package']) && empty($_SESSION['renew-insurance']) && isset($order) && (!$order->has_status( 'failed' ))){
							$expire_data = array('plan_id'=>$plan_ids,'user_id'=>$p_id,'order_id'=>$order->get_order_number(),'reffer_id'=>$p_id,'order_date'=>$st,'expire_date'=>$end,'ip_address'=>$ip);
							$expire_data_type = array('%d','%d','%d','%d','%s','%s','%s');
							$wpdb->insert($expire,$expire_data,$expire_data_type);
						}
						if($flag == 1  &&  empty($_SESSION['renew-package']) && empty($_SESSION['renew-insurance']) && isset($order)  && (!$order->has_status( 'failed' ))){

							mailToPackageUser($order->get_order_number(),$order->get_formatted_order_total(),$order->get_date_created(),1);
						}
						if($flag == 0 &&  empty($_SESSION['renew-package']) && empty($_SESSION['renew-insurance']) && isset($order) && (!$order->has_status( 'failed' ))){

							mailToPackageUser($order->get_order_number(),$order->get_formatted_order_total(),$order->get_date_created(),0);
						}
						if(isset($_SESSION['renew-package']) && !empty($_SESSION['renew-package']) && empty($_SESSION['renew-insurance']) && isset($order) && (!$order->has_status( 'failed' ))) {

							$sql_update_renew = "update wp_expire_plan set order_date = '".$st."',expire_date = '".$end."',ip_address = '".$ip."' where id = '".$_SESSION['expire_id']."'";
							$wpdb->query($sql_update_renew);

							mailToRenewUser($order->get_order_number(),$order->get_formatted_order_total(),$order->get_date_created());
							unset($_SESSION['renew-package']);
							unset($_SESSION['expire_id']);
						 }

						if(isset($_SESSION['renew-insurance']) && !empty($_SESSION['renew-insurance']) && empty($_SESSION['renew-package']) && isset($order) && (!$order->has_status( 'failed' ))) {

							if($_SESSION['user_idd'] == $p_id){
							$end_dd = date('Y-m-d h:i:s', strtotime('+1 years'));
							$sql_update_renew = "update wp_notary_insurance set currentt_date = '".$st."',ip_address = '".$ip."',expire_date='".$end_dd."' where id = '".$_SESSION['insurance_id']."' and u_id = '".$p_id."'";
							$wpdb->query($sql_update_renew);
							}
							mailToRenewInsurance($order->get_order_number(),$order->get_formatted_order_total(),$order->get_date_created());
							unset($_SESSION['insurance_id']);
							unset($_SESSION['renew-insurance']);
							unset($_SESSION['user_idd']);
						 }
					}
				   if(($manager_detail) && isset($order) && (!$order->has_status( 'failed' )) ){
					 /*==SELECTED PACKAGE DETAIL==*/
				     $plan = wc_get_product($manager_detail[0]->plan_id);
				     /*==EMAIL TO ALL MEMBER FOR HIS/HER PACKAGE DETAIL==*/
				     if($member_detail) {
				     foreach($member_detail as $member){
				     $to = $member->email;
				     $subject = "Pacakge detail";
					$message = '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0"><title>Emailer</title><style>#outlook a{padding:0;} body{width:100% !important; background-color:#fff;-webkit-text-size-adjust:none; -ms-text-size-adjust:none;margin:0 !important; padding:0 !important;}  .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} ol li {margin-bottom:15px;} img{height:auto; line-height:100%; outline:none; text-decoration:none;} #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;} p {margin: 1em 0;} h1, h2, h3, h4, h5, h6 {color:#222222 !important; font-family:Arial, Helvetica, sans-serif; line-height: 100% !important;} table td {border-collapse:collapse;} .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;} .im {color:black;} div[id="tablewrap"] { width:100%; max-width:600px!important; } table[class="fulltable"], td[class="fulltd"] { max-width:100% !important; width:100% !important; height:auto !important; } @media screen and (max-device-width: 430px), screen and (max-width: 430px) { p, table, table tr, table td {font-size:18px !important;}td[class=emailcolsplit]{ width:100%!important; float:left!important; padding-left:0!important; max-width:430px !important; } td[class=emailcolsplit] img { margin-bottom:20px !important; } } </style> </head> <body><table id="backgroundTable" width="100%" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:14px;line-height:1.4;background:#fff;height:auto !important; margin:0; padding:0; width:100% !important;"> <tr> <td align="center" style="padding-bottom:20px;"> <a href="'.get_site_url().'"> <img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /> </a> </td> </tr> <tr> <td align="center" valign="top"> <div id="tablewrap" style="width:100% !important; max-width:600px !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important;"> <table id="contenttable" width="600" align="center" cellpadding="20" cellspacing="0" border="0" style="background-color:#f7f7f7;margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:600px !important;-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);"> <tr> <td width="100%"> <table width="100%" border="0" cellspacing="0" cellpadding="10"> <tr> <td width="100%" align="left" colspan="2"> <h4 style="font-size:21px; margin:0 !important; margin-bottom:15px !important; text-transform:capitalize;">Hi '.$member->name.',</h4> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">I am pleased to inform that you have joined us as a <strong>Group Member </strong> of a package. Your membership package details as follows:</p> </td> </tr> <tr> <td width="100%" align="left" colspan="2" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0 10px; font-weight:normal;"><strong>Package Duration: </strong>4 Year</td> </tr> <tr> <td width="100%" colspan="2"> <table width="100%" border="0" cellspacing="0" cellpadding="5" style="color:#000;"> <tr> <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 0; font-weight:normal;"><strong>Package:  </strong>'.$plan->name.'</td> </tr> <tr> <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 0; font-weight:normal;"><strong>Group Manager Name:  </strong>'.$manager_detail[0]->name.' ('.$manager_detail[0]->email.')</td> </tr> </table> </td> </tr>';
					 $product_ids2 = json_decode($member->add_product);
					 if(!in_array(230,$product_ids2)){
						 array_unshift($product_ids2,230,138);
					 }
					 $variation_prod2 = json_decode($member->product_variations);
					 if(!empty($product_ids2)){
					 $message.= '<tr><td width="100%" colspan="2" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0 10px; font-weight:normal;"><strong>Product details:</strong></td></tr>';
					 $message.= '<tr><td colspan="2" width="100%"><div class="fullWidth" style="width:100% !important; overflow-x:auto !important;"><table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee; min-width:400px !important;">';
					 $message.= '<tr bgcolor="#a60311" style="border:1px solid #eee;color:#fff;"><th width="10%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;"><strong>S.N</strong></th><th width="90%" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;"><strong>Product Name</strong></th></tr>';
					 $a=0; foreach($product_ids2 as $item2){ $a++;
					 $item_detail2 = wc_get_product($item2);
					 if(!empty($item_detail2)){
					 $message.= '<tr>';
					 $message.= '<td valign="top" width="10%" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;">'.$a.'</td>';
					 $message.= '<td valign="top" width="90%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;">'.$item_detail2->name;
					 foreach( $variation_prod2 as $value2){
					 $name_count3 = count($value2->$item2);
					 $gl=0; foreach($value2->$item2 as $item_price2){ $gl++;
					 if($name_count3 == 1){
					 $message.= '<br/> <strong> Policy </strong> : $'.ucfirst($item_price2);
					 }else {
					 if($gl==1){
					 $message.='<br/> <strong> Color </strong> : '.ucfirst($item_price2);
					 }else{
					 $message.= '<br/> <strong> Shape </strong> : '.ucfirst($item_price2);
					 } } } }
					 $message.= '</td></tr>';
					 } }
					 $message.= '</table></div></td></tr>';
					 }
					 $message.= '</table></td> </tr> <tr> <td valign="top bottom" style="padding-left:30px; padding-right:30px;"> <div class="thanks-note"> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">If you have any questions, please do not hesitate to <a href="'.get_site_url().'/contact-us/">contact us</a>.</p> <p>We&#39;re here to help! Thank you for your business.</p> </div> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">Andrew Whitfield<br>Founder</p> </td> </tr> </table> </div> </td> </tr> <tr> <td height="40" align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;"> <a href="'.get_site_url().'">Texas Notary Solutions</a> </td> </tr> </table> </body></html>';
				     $header = "From:Texas Notary Solutions <info@texasnotarysolutions.com> \r\n";
				     $header .= "MIME-Version: 1.0\r\n";
					 $header .= "Content-type: text/html\r\n";
					 //mail($to,$subject,$message,$header);
				     } }
				   /*==EMAIL TO GROUP MANAGER==*/
				   $manager_email = $manager_detail[0]->email;
				   $subject2 = "Your Package Subscription Summary";
				   $ft_name = explode(' ',$manager_detail[0]->name);
				   $msg_for_allg="";
				   foreach ($order->get_items() as $item_id => $item_data) {
					    $product = $item_data->get_product();
					    $product_name = $product->get_name();
					    $item_quantity = $item_data->get_quantity();
					    $item_total = $item_data->get_total();
						if(!empty(get_post_meta( $product->get_id(), "wpcf-product_type", true))){

							$package_content = get_post_meta( $product->get_id(), "wpcf-package_content", true);
							$package_content_prices = get_post_meta( $product->get_id(), "wpcf-prices", true);

							$package_content = explode(',',$package_content);
							$package_content_prices = explode(',',$package_content_prices);

							foreach($package_content as $keyy=>$val_pkg){
							$msg_for_allg .= '<br /><span style="padding-left:15px;padding-top:5px;">'.$val_pkg.': <strong>'.wc_price(number_format($package_content_prices[$keyy],2)).'</strong></span>';
							}
						}
					}
				   $txt = '<!DOCTYPE html> <html> <head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Emailer</title> <style> #outlook a{padding:0;} body{width:100% !important; background-color:#fff;-webkit-text-size-adjust:none; -ms-text-size-adjust:none;margin:0 !important; padding:0 !important;}  .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} ol li {margin-bottom:15px;} img{height:auto; line-height:100%; outline:none; text-decoration:none;} #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;} p {margin: 1em 0;} h1, h2, h3, h4, h5, h6 {color:#222222 !important; font-family:Arial, Helvetica, sans-serif; line-height: 100% !important;} table td {border-collapse:collapse;} .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;} .im {color:black;} div[id="tablewrap"] { width:100%; max-width:600px!important; } table[class="fulltable"], td[class="fulltd"] { max-width:100% !important; width:100% !important; height:auto !important; } @media screen and (max-device-width: 430px), screen and (max-width: 430px) { p, table, table tr, table td {font-size:18px !important;} td[class=emailcolsplit]{ width:100%!important; float:left!important; padding-left:0!important; max-width:430px !important; } td[class=emailcolsplit] img { margin-bottom:20px !important; } } </style> </head> <body><table id="backgroundTable" width="100%" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:14px;line-height:1.4;background:#fff;height:auto !important; margin:0; padding:0; width:100% !important;"><tr> <td align="center" style="padding-bottom:20px;"> <a href="'.get_site_url().'"> <img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /> </a> </td> </tr> <tr> <td align="center" valign="top"> <div id="tablewrap" style="width:100% !important; max-width:600px !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important;"> <table id="contenttable" width="600" align="center" cellpadding="20" cellspacing="0" border="0" style="background-color:#f7f7f7;  margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:600px !important;-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);"> <tr> <td width="100%"> <table width="100%" border="0" cellspacing="0" cellpadding="10"> <tr> <td width="100%" align="left" colspan="4"> <h4 style="font-size:21px; margin:0 !important; margin-bottom:15px !important; text-transform:capitalize;">Hi '.$ft_name[0].',</h4> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">Thank you for your business! Please find a summary of your order below. A receipt was also sent to this email address.</p> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;"><p>All the people that you signed up will receive an email with a link to get started with their notary application. If they don\'t see this email in the inbox, please have them check their junk mail folder.</p><p>You can track the progress of applications on your <a href="'.get_site_url().'/dashboard/">Dashboard</a> page. From the Dashboard page, you can also reset member passwords, modify names, and sign-up additional people to become a notary. The free (and paid) training is also available through the <a href="'.get_site_url().'/dashboard/">Dashboard</a>.</p><p>Here is a recap of your order. Should you need help or anything else, please do not hesitate to contact us.</p><p>Your membership package details:</p> </td> </tr> <tr> <td width="100%" colspan="4"> <table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#fff" style="border:1px solid #eee; color:#000;"> <tr> <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Order: </strong>#'.$order->get_id().'</td> </tr> <tr> <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Total Amount: </strong>'.$order->get_formatted_order_total().'</td> </tr> </table> </td> <tr> <td width="100%" colspan="4"> <table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#fff" style="border:1px solid #eee;color:#000;"> <tr> <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Package : </strong>'.$plan->name.$msg_for_allg.'</td> </tr> <tr> <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Package Duration: </strong>4 Year</td> </tr> </table> </td></tr>';
					 $product_ids = json_decode($manager_detail[0]->add_product);
					 if(!in_array(230,$product_ids)){
						 array_unshift($product_ids,230,138);
					 }
					 $variation_prod = json_decode($manager_detail[0]->product_variations);
					 if(!empty($product_ids)){
					 $txt.= '<tr><td width="100%" colspan="4" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;"><strong>Product Details:</strong></td></tr>';
					 $txt.= '<tr><td width="100%" colspan="4"><div class="fullWidth" style="width:100% !important; overflow-x:auto !important;"><table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee; min-width:400px !important;">';
					 $txt.= '<tr bgcolor="#a60311" style="color:#fff;"><th width="10%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;"><strong>S.N</strong></th><th width="90%" align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;text-align:left;"><strong>Product Name</strong></th></tr>';
					 $i=0;foreach($product_ids as $item){
					 $item_detail = wc_get_product($item);
					 if(!empty($item_detail)){ $i++;
					 $txt.= '<tr>';
					 $txt.= '<td width="10%" valign="top" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;">'.$i.'</td>';
					 $txt.= '<td width="90%" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;">'.$item_detail->name;
					 foreach( $variation_prod as $value){
					 $name_count = count($value->$item);
					 $g=0; foreach($value->$item as $item_price){ $g++;
					 if($name_count == 1){
					 $txt.= '<br/> <strong> Policy </strong> : $'.ucfirst($item_price);
					 }else {
					 if($g==1){
					 $txt.='<br/> <strong> Color </strong> : '.ucfirst($item_price);
					 }else{
					 $txt.= '<br/> <strong> Shape </strong> : '.ucfirst($item_price);
					  } }  } }
					 $txt.= '</td></tr>';
					 } }
					 $txt.= '</table></div></td></tr>';
					 }
				   if(!empty($member_detail)){
					$txt.='<tr><td width="100%" colspan="4" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"> <strong>Your Group Member(s) details:</strong> </td></tr>';
					foreach($member_detail as  $members){
					$product_ids2 = json_decode($members->add_product);
					if(!in_array(230,$product_ids2)){
						 array_unshift($product_ids2,230,138);
					 }
					$variation_prod2 = json_decode($members->product_variations);
					if(!empty($product_ids2)){
					$txt.= '<tr>
								<td width="100%" colspan="4">
									<table width="100%" bgcolor="#fff" style="border:1px solid #eee;color:#000">
										<tbody>
											<tr>
												<td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Name :</strong> '.$members->name.'</td>
											</tr>
											<tr>
												<td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Email :</strong> '.$members->email.'</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>';
					$txt.= '<tr><td width="100%" colspan="4" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;"><strong>Product Details: </strong></td></tr>';
					$txt.= '<tr><td width="100%" colspan="4"><div class="fullWidth" style="width:100% !important; overflow-x:auto !important;"><table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee; min-width:400px !important;">';
					$txt.= '<tr bgcolor="#a60311" style="border:1px solid #eee;color:#fff";><th width="10%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;"><strong>S.N</strong></th><th align="90%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;text-align:left;"><strong>Product Name</strong></th></tr>';
					$j=0;foreach($product_ids2 as $item2){
					$item_detail2 = wc_get_product($item2);
					if(!empty($item_detail2)){
						 $j++;
					$txt.= '<tr>';
					$txt.= '<td width="10%" valign="top" align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;">'.$j.'</td>';
					$txt.= '<td width="90%" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;">'.$item_detail2->name;
					foreach( $variation_prod2 as $value2){
					$name_count2 = count($value2->$item2);
					$g2=0; foreach($value2->$item2 as $item_price2){ $g2++;
					if($name_count2 == 1){
					$txt.= '<br/> <strong> Policy </strong> : $'.ucfirst($item_price2);
					}else {
					if($g2==1){
					$txt.='<br/> <strong> Color </strong> : '.ucfirst($item_price2);
					}else{
					$txt.= '<br/> <strong> Shape </strong> : '.ucfirst($item_price2);
					 } } } }
					$txt.= '</td></tr>';
					} }
					$txt.= '</table></div></td></tr>';
					} } }
					$txt.= '</table></td></tr><tr><td valign="top" style="padding-left:30px; padding-right:30px;">';
				   if(!empty($product_ids)){
				   $txt.= '<p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">Please follow the link below to complete the notary application form.<br><a style="margin-top:10px;display: inline-block !important;text-decoration: none;text-align: center;padding: 10px !important;color: #fff;background: #334667;" href="'.get_site_url().'/group-notary-application-form/?notary_id='.base64_encode($group_notary_id).'">Complete The Notary Application </a></p>';
				   }
				   $txt.= '<div class="thanks-note"><p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">If we can be of further assistance, please let us know. We&rsquo;re here to help!</p> </div> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">Andrew Whitfield<br>Founder</p> </td> </tr> </table> </div> </td> </tr> <tr> <td align="center" valign="top"  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;" height="40"> <a href="'.get_site_url().'">Texas Notary Solutions</a> </td> </tr></table></body></html>';
				   $header = "From:Texas Notary Solutions <info@texasnotarysolutions.com> \r\n";
				   $header .= "MIME-Version: 1.0\r\n";
				   $header .= "Content-type: text/html\r\n";
				   //mail($manager_email,$subject2,$txt,$header);
				   wp_mail( $manager_email, $subject2, $txt, $header);
				   //wp_mail( $to, $subject, $message, $headers, $attachments );
					/*==EMAIL TO SITE ADMIN==*/
				   $admin = get_option( 'admin_email' );
				   $subject3 = "Group Package Oder Detail( #".$order->get_id().")";
					$msg = '<!DOCTYPE html> <html> <head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Emailer</title> <style> #outlook a{padding:0;} body{width:100% !important; background-color:#fff;-webkit-text-size-adjust:none; -ms-text-size-adjust:none;margin:0 !important; padding:0 !important;}  .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} ol li {margin-bottom:15px;} img{height:auto; line-height:100%; outline:none; text-decoration:none;} #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;} p {margin: 1em 0;} h1, h2, h3, h4, h5, h6 {color:#222222 !important; font-family:Arial, Helvetica, sans-serif; line-height: 100% !important;} table td {border-collapse:collapse;} .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;} .im {color:black;} div[id="tablewrap"] { width:100%; max-width:600px!important; } table[class="fulltable"], td[class="fulltd"] { max-width:100% !important; width:100% !important; height:auto !important; } @media screen and (max-device-width: 430px), screen and (max-width: 430px) {p, table, table tr, table td {font-size:18px !important;} td[class=emailcolsplit]{ width:100%!important; float:left!important; padding-left:0!important; max-width:430px !important; } td[class=emailcolsplit] img { margin-bottom:20px !important; } } </style> </head> <body><table id="backgroundTable" width="100%" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:14px;line-height:1.4;background:#fff;height:auto !important; margin:0; padding:0; width:100% !important;"> <tr> <td align="center" style="padding-bottom:20px;"> <a href="'.get_site_url().'"> <img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /> </a> </td> </tr> <tr> <td align="center" valign="top"> <div id="tablewrap" style="width:100% !important; max-width:600px !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important;"> <table id="contenttable" width="600" align="center" cellpadding="20" cellspacing="0" border="0" style="background-color:#f7f7f7; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:600px !important;-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);"> <tr> <td width="100%"> <table width="100%" border="0" cellspacing="0" cellpadding="10"> <tr> <td width="100%" align="left" colspan="4"> <h4 style="font-size:21px; margin:0 !important; margin-bottom:15px !important;text-transform:capitalize;">Hi Admin,</h4> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">'.$ft_name[0].'(Group Manager) has purchase <strong>'.$plan->name.'</strong> package. His/Her package details as follows:</p> </td> </tr> <tr> <td width="100%" colspan="4"> <table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#fff" style="border:1px solid #eee; color:#000;"> <tr> <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Order:</strong> #'.$order->get_id().'</td> </tr> <tr> <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Total Amount:</strong> '.$order->get_formatted_order_total().'</td> </tr> <tr> <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Order Date :</strong> '.wc_format_datetime( $order->get_date_created() ).'</td> </tr> <tr> <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Package :</strong> '.$plan->name.'</td> </tr>';
					$product_ids = json_decode($manager_detail[0]->add_product);
				   if(!in_array(230,$product_ids)){
				       array_unshift($product_ids,230,138);
					}
				   $variation_prod = json_decode($manager_detail[0]->product_variations);
				   if(!empty($product_ids)){
					 $msg.= '
							<tr>
								 <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Name :</strong> '.$manager_detail[0]->name.'</td>
							 </tr>
							 <tr>
								 <td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Email :</strong> '.$manager_detail[0]->email.'</td>
							</tr>
						';
					 $msg.= '</table></td></tr><tr><td width="100%" colspan="4"><div class="fullWidth" style="width:100% !important; overflow-x:auto !important;"><table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee; min-width:400px !important;">   ';
					 $msg.= '<tr bgcolor="#a60311" style="border:1px solid #eee; color:#fff"><th width="10%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;"><strong>S.N</strong></th><th align="left" width="90%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;text-align:left;"><strong>Product Name</strong></th></tr>';
					 $i=0;foreach($product_ids as $item){
					 $item_detail = wc_get_product($item);
				     if(!empty($item_detail)){ $i++;
				     $msg.= '<tr>';
				     $msg.= '<td width="10%" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;">'.$i.'</td>';
				     $msg.= '<td width="90%" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;">'.$item_detail->name;
				     foreach( $variation_prod as $value){
					 $name_count = count($value->$item);
					 $g=0; foreach($value->$item as $item_price){ $g++;
					 if($name_count == 1){
					 $msg.= '<br/> <strong> Policy </strong> : $'.ucfirst($item_price);
					 }else {
					 if($g==1){
					 $msg.='<br/> <strong> Color </strong> : '.ucfirst($item_price);
					 }else{
					 $msg.= '<br/> <strong> Shape </strong> : '.ucfirst($item_price);
					 } } } }
				     $msg.= '</td></tr>';
					 } }
					 $msg.= '</table></div></td></tr>';
					 }
					  if(!empty($member_detail)){
					  $msg.=' <tr><td width="100%" colspan="4" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Group Member(s) details:</strong> </td></tr>';
					  foreach($member_detail as  $members){
					  $product_ids2 = json_decode($members->add_product);
					  if(!in_array(230,$product_ids2)){
						 array_unshift($product_ids2,230,138);
					   }
					  $variation_prod2 = json_decode($members->product_variations);
					  if(!empty($product_ids2)){
					  $msg.= '<tr>
								<td width="100%" colspan="4">
									<table width="100%" bgcolor="#fff" style="border:1px solid #eee; color:#000">
										<tbody>
											<tr>
												<td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Name :</strong> '.$members->name.'</td>
											</tr>
											<tr>
												<td width="100%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px 15px; font-weight:normal;"><strong>Email :</strong> '.$members->email.'</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>';
					  $msg.= '<tr><td width="100%" colspan="4"><div class="fullWidth" style="width:100% !important; overflow-x:auto !important;"><table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee; min-width:400px !important;">';
					  $msg.= ' <tr bgcolor="#a60311" style="border:1px solid #eee; color:#fff"><th width="10%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;"><strong>S.N</strong></th><th align="left" width="90%" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;text-align:left;"><strong>Product Name</strong></th></tr>';
					  $j=0;foreach($product_ids2 as $item2){
					  $item_detail2 = wc_get_product($item2);
				      if(!empty($item_detail2)){
						  $j++;
				      $msg.= '<tr>';
				      $msg.= '<td width="10%" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;">'.$j.'</td>';
				      $msg.= '<td width="90%" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:10px; font-weight:normal;">'.$item_detail2->name;
				      foreach( $variation_prod2 as $value2){
					  $name_count2 = count($value2->$item2);
					  $g2=0; foreach($value2->$item2 as $item_price2){ $g2++;
					  if($name_count2 == 1){
						   $msg.= '<br/> <strong> Policy </strong> : $'.ucfirst($item_price2);
					  }else {
				      if($g2==1){
						$msg.='<br/> <strong> Color </strong> : '.ucfirst($item_price2);
					  }else{
						$msg.= '<br/> <strong> Shape </strong> : '.ucfirst($item_price2);
					  } } } }
				      $msg.= '</td></tr>';
					   } }
					 $msg.= '</table></div></td></tr>';
					 } } }
				     $msg.= '</table></td></tr></table></div></td></tr><tr><td width="100%" align="center" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;"><a href="'.get_site_url().'">Texas Notary Solutions</a></td></tr></table></body></html>';
				      $header2 = "From:Texas Notary Solutions <info@texasnotarysolutions.com> \r\n";
				      $header2 .= "MIME-Version: 1.0\r\n";
					  $header2 .= "Content-type: text/html\r\n";
					  //mail($admin,$subject3,$msg,$header2);
					  wp_mail( $admin, $subject3, $msg, $header2);
				  }
				/*==DELETE ALL ACTIVE CUSTOM SESSION VARIABLE==*/
				if(!empty($_SESSION['parent'])){
					$parent = $_SESSION['parent'];
					/*==DELETE TEMPORARY DATA FROM TEMPORARY TABLE==*/
					$delete_res = $wpdb->delete( $table_name , array( 'ID' => $parent ) );
					if($delete_res){
						$wpdb->delete( $table_name , array( 'parent_id' => $parent ) );
					}
					/*$_SESSION['parent_checkout']=$parent;*/
					unset($_SESSION['parent']);
				}

				


				?>

				

			<?php endif; ?>
			<?php //do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
			<?php //do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
	   <?php else : ?>
			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( '<h1>Thank You.</h1><br><p>Thank you for your order. Click the button to start and file the state\'s application. This will take about 5 minutes. You may also complete the application later by signing into your account.</p>', 'woocommerce' ), null ); ?></p>
	   <?php endif; ?>
	</div>
<?php if ( $order ) : ?>
	<?php if ( !$order->has_status( 'failed' ) ) : ?>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('#save_download_form1').click(function(){
				var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
				var current_dates = "<?php echo date("m-d-Y"); ?>";
				var action=jQuery(".action").val();
				var first_name="<?php echo $current_user->user_firstname; ?>";
				var middle_name="<?php echo $current_user->user_middlename; ?>";
				var last_name="<?php echo $current_user->user_lastname; ?>";
				var save_for_later=jQuery(this).val();
				var address="<?php echo get_user_meta( $current_user->ID, 'billing_address_1', true ).' '.get_user_meta( $current_user->ID, 'billing_address_2', true ); ?>";
				var email="<?php echo $current_user->user_email; ?>";
				var data = {
					'action': action,
					'first_name'  : first_name,
					'middle_name'  : middle_name,
					'last_name'  : last_name,
					'ssn_number'  : '',
					'mailing_address'  : address,
					'email_address'  : email,
					'city'  : '',
					'zipcode'  : '',
					'dob'  : '',
					'dl_no' : '',
					'issuse_state' : '',
					'chk_a' : '',
					'renew_check' : '',
					'renew_expire_date' : '',
					'current_dates' : current_dates,
					'order_id' : "<?php echo $order->get_order_number(); ?>",
					'user_id': "<?php echo $current_user->id; ?>",
					'update_notary_id' : '',
					'save_for_later' : save_for_later
				};
				jQuery.post(ajaxurl, data, function(response) {

						jQuery('#abc').html('<div class="woocommerce-message success">Form has been saved.</div>');
						 window.setTimeout(function(){
		                window.location.href="<?php echo get_site_url();?>/dashboard/notary-form-status/"}, 2000);
				});


			});
		})

		jQuery.cookie("gpackagesbtn", null, {expires: -1, path: '/' });
		jQuery("#unsetcookie").css("display","none");
	</script>
<?php endif; endif; ?>
<?php 

function mailToRenewInsurance($order_no,$total_formated,$date_created){

	global $wpdb;
	$order = wc_get_order( $order_no );
	$order_data = $order->get_data();
	$from = 'Texas Notary Solutions <info@texasnotarysolutions.com>';
	$header  = "From: $from \n";
	$header .= "MIME-Version: 1.0" . "\r\n";
	$header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$subject = "Your Texas Notary Solutions order receipt from ".date('F-d-Y',strtotime($date_created));
	$current_user = wp_get_current_user();
	$msg = '<!DOCTYPE html> <html> <head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Emailer</title> <style> #outlook a{padding:0;} body{width:100% !important; background-color:#fff;-webkit-text-size-adjust:none; -ms-text-size-adjust:none;margin:0 !important; padding:0 !important;}  .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} ol li {margin-bottom:15px;} img{height:auto; line-height:100%; outline:none; text-decoration:none;} #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;} p {margin: 1em 0;} h1, h2, h3, h4, h5, h6 {color:#222222 !important; font-family:Arial, Helvetica, sans-serif; line-height: 100% !important;} table td {border-collapse:collapse;} .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;} .im {color:black;} div[id="tablewrap"] { width:100%; max-width:600px!important; } table[class="fulltable"], td[class="fulltd"] { max-width:100% !important; width:100% !important; height:auto !important; } @media screen and (max-device-width: 430px), screen and (max-width: 430px) {p, table, table tr, table td {font-size:18px !important;} td[class=emailcolsplit]{ width:100%!important; float:left!important; padding-left:0!important; max-width:430px !important; } td[class=emailcolsplit] img { margin-bottom:20px !important; } } </style> </head> <body><table id="backgroundTable" width="100%" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:14px;line-height:1.4;background:#fff;height:auto !important; margin:0; padding:0; width:100% !important;"><tr> <td align="center" style="padding-bottom:20px;"> <a href="'.get_site_url().'"> <img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /> </a> </td> </tr> <tr> <td align="center" valign="top"> <div id="tablewrap" style="width:100% !important; max-width:600px !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important;"> <table id="contenttable" width="600" align="center" cellpadding="20" cellspacing="0" border="0" style="background-color:#f7f7f7; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:600px !important;-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);"> <tr> <td width="100%"> <table width="100%" border="0" cellspacing="0" cellpadding="10"> <tr> <td width="100%" align="left" colspan="2"> <h4 style="font-size:21px; margin:0 !important; margin-bottom:15px !important;text-transform:capitalize;">Hi '.$current_user->user_firstname.',</h4> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">Thank you for your business! Please find your receipt below.</p> </td> </tr> <tr> <td width="100%" valign="top">
	<p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;"><strong>Order #</strong>'.$order_no.'</p>';
      if($order_data['status'] == 'on-hold'){
		 $msg .= '<p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">Your order is on-hold until we confirm payment has been received. Your order details are shown below for your reference</p>';
	  }
	$msg .= '<p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">Your Notary E/O Insurance Renewal is renewed. </p> </td> </tr> </table> </td></tr>';
    $msg_for_all .= '<tr bgcolor="#a60311"><th width="60%" style="text-align:left;color:#fff;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Product</th><th width="10%" style="text-align:left;color:#fff;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Quantity</th><th width="30%" style="text-align:left;color:#fff;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Price</th></tr>';
    $toatal_amount = 0;
    foreach ($order->get_items() as $item_id => $item_data) {
    $product = $item_data->get_product();
    $product_name = $product->get_name();
    $item_quantity = $item_data->get_quantity();
    $item_total = $item_data->get_total();
    $msg_for_all .= '<tr><td width="60%" style="text-align:left;vertical-align:middle;border:1px solid #eee;word-wrap:break-word;color:#636363;padding:12px;font-size:14px;line-height:19px;">'.$product_name.'</td><td width="10%" style="text-align:left;vertical-align:middle;border:1px solid #eee;color:#636363;padding:12px;font-size:14px;line-height:19px;">'.$item_quantity.'</td><td width="30%" style="text-align:left;vertical-align:middle;border:1px solid #eee;color:#636363;padding:12px;font-size:14px;line-height:19px;">'.wc_price(number_format( $item_total, 2 )).'</td></tr>';
    $toatal_amount = $toatal_amount + $item_total;
	}
	$msg_for_all .= '<tr><td colspan="2" style="text-align:left;border-top-width:4px;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Subtotal:</td><td  style="text-align:left;border-top-width:4px;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">'.wc_price(number_format($toatal_amount,2)).'</td></tr>';
	$msg_for_all .= '<tr><td colspan="2" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Shipping:</td><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">'.wc_price(number_format($order_data['shipping_total'],2)).'</td></tr>';
	$msg_for_all .= '<tr><td colspan="2" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">State Tax:</td><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">'.wc_price(number_format($order_data['total_tax'],2)).'</td></tr>';
	$msg_for_all .= '<tr><td colspan="2" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Payment method:</td><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">'.$order_data['payment_method_title'].'</td></tr>';
	$msg_for_all .= '<tr><td colspan="2" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Total:</td><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">'.$total_formated.'</td></tr></table></div>';

	$msg_for_all .= '<table  cellspacing="0" bgcolor="#f7f7f7" cellpadding="0" style="width:100%;vertical-align:top" border="0"><tbody><tr> <td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px" valign="top" width="50%"> <h3 style="color:#a60311;display:block;font-family:Helvetica Neue,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height: 25px;margin:16px 0 8px;text-align:left;">Billing Address</h3> <p style="color:#3c3c3c;font-family:Helvetica Neue;,Helvetica,Roboto,Arial,sans-serif;margin:0 0 16px; font-size:14px; line-height:19px;">'.$order_data['billing']['first_name'].' '.$order_data['billing']['last_name'];if(!empty($order_data['billing']['company'])){$msg_for_all .= '<br>';} $msg_for_all .=$order_data['billing']['company'].'<br>'.$order_data['billing']['address_1'];if(!empty($order_data['billing']['address_2'])){$msg_for_all .='<br><span style="text-decoration:none;color:#000;">'.$order_data['billing']['address_2'].'</span>';}$msg_for_all .='<br>'.$order_data['billing']['city'].'<br>'.$order_data['billing']['state'].',&nbsp;'.$order_data['billing']['postcode'].'<br>'.$order_data['billing']['country'].'<br><span style="text-decoration:none;color:#000;">'.$order_data['billing']['email'].'</span><br>'.$order_data['billing']['phone'].'</p> </td> <td width="50%" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px"> <h3  style="color:#a60311;display:block;font-family:Helvetica Neue,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height: 25px;margin:16px 0 8px;text-align:left;">Shipping Address</h3> <p style="color:#3c3c3c;font-family:Helvetica Neue;,Helvetica,Roboto,Arial,sans-serif;margin:0 0 16px; font-size:14px; line-height:19px;">'.$order_data['shipping']['first_name'].' '.$order_data['shipping']['last_name'];if(!empty($order_data['shipping']['company'])){$msg_for_all .= '<br>';} $msg_for_all .=$order_data['shipping']['company'].'<br>'.$order_data['shipping']['address_1'];if(!empty($order_data['shipping']['address_2'])){$msg_for_all .= '<br>';} $msg_for_all .=$order_data['shipping']['address_2'].'<br>'.$order_data['shipping']['city'].'<br>'.$order_data['shipping']['state'].',&nbsp;'.$order_data['shipping']['postcode'].'<br>'.$order_data['shipping']['country'].'<br>'.$order_data['shipping']['email'].'<br>'.$order_data['shipping']['phone'].'</p> </td> </tr> </tbody> </table> </td></tr>';
	$msg .= $msg_for_all;
    $msg .= '<tr><td width="100%" align="center" valign="top" style="padding:20px 0;"><a href="'.get_site_url().'">Texas Notary Solutions</a></td></tr></table>';

    $admin_subject = '[Texas Notary Solutions] New customer order '.($order_no).' - '.date('F-d-Y',strtotime($date_created));
     $msg_admin = '<table id="backgroundTable" width="100%" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:14px;line-height:1.4;background:#fff;height:auto !important; margin:0; padding:0; width:100% !important;"><tbody> <tr> <td align="center" valign="top"> <table width="600px" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding-top:20px;"> <tr> <td align="center" style="padding-bottom:20px;"> <a href="'.get_site_url().'"> <img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /> </a> </td> </tr> <tr> <td align="center" valign="top"> <div id="tablewrap" style="width:100% !important; max-width:600px !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important;"> <table id="contenttable" width="600" align="center" cellpadding="20" cellspacing="0" border="0" style="background-color:#f7f7f7; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:600px !important;-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);"> <tr> <td width="100%"> <table width="100%" border="0" cellspacing="0" cellpadding="10"> <tr> <td width="100%" valign="top" colspan="2" style="padding:12px;"><p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">Order #'.$order_no.'</p>';
     /*$msg_admin .= 'You have received an order from '.$current_user->user_firstname.' '.$current_user->user_lastname.'.The order is as follows';*/
	$msg_admin .= $msg_for_all;
    $msg_admin .= '</td></tr> </table> </td> </tr> </table> </div> </td> </tr> <tr> <td align="center" valign="top" style="padding:20px 0;"> <a href="'.get_site_url().'">Texas Notary Solutions</a> </td> </tr> </table> </td> </tr> </tbody> </table></body></html>';
	$retval_user = wp_mail($current_user->user_email,$subject,$msg,$header);
	$retval_admin = wp_mail(get_option( 'admin_email' ),$admin_subject,$msg_admin,$header);

}


// Renew User Mail Format
function mailToRenewUser($order_no,$total_formated,$date_created){

	global $wpdb;
	$order = wc_get_order( $order_no );
	$order_data = $order->get_data();
	$from = 'Texas Notary Solutions <info@texasnotarysolutions.com>';
	$header  = "From: $from \n";
	$header .= "MIME-Version: 1.0" . "\r\n";
	$header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$subject = "Your Texas Notary Solutions order receipt from ".date('F-d-Y',strtotime($date_created));
	$current_user = wp_get_current_user();
	$msg = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:13px;line-height:1.4;background:#fff;"><tr><td align="center" style="padding-bottom:20px;"><a href="'.get_site_url().'"><img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /></a></td></tr><tr><td align="center" valign="top">
     <table width="600" border="0" cellspacing="0" cellpadding="10" bgcolor="#f7f7f7" style="-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);">
     <tr><td><table width="100%" border="0" cellspacing="0" cellpadding="10"><tr>
     <td align="left" colspan="2"><h4 style="text-transform:capitalize;">Hi '.$current_user->user_firstname.',</h4><p style="margin-bottom:0;font-size:16px;">Thank you for your business! Please find your receipt below.</p></td></tr>
     <tr>
     <td valign="top">
     <p><b>Order #</b>'.$order_no.'</p>';
      if($order_data['status'] == 'on-hold'){
		 $msg .= '<p style="color:#000;">Your order is on-hold until we confirm payment has been received. Your order details are shown below for your reference</p>';
	  }

     $msg .= '<p style="color:#000;">Please follow the link below to complete the notary application form.<br><a style="margin-top:10px;display: inline-block !important;text-decoration: none;text-align: center;padding: 10px !important;color: #fff;background: #334667;" href="'.get_site_url().'/notary-application-form?o_id='. base64_encode($order_no).'">Complete The Notary Application</a></p>';

     $msg .= '<p>Your Package is renewed </p></td></tr></table></td></tr>';

    $msg_for_all .= '<tr bgcolor="#a60311"><th style="text-align:left;color:#fff;border:1px solid #e5e5e5;padding:12px">Product</th><th style="text-align:left;color:#fff;border:1px solid #e5e5e5;padding:12px">Quantity</th><th style="text-align:left;color:#fff;border:1px solid #e5e5e5;padding:12px">Price</th></tr>';
    $toatal_amount = 0;
    foreach ($order->get_items() as $item_id => $item_data) {
    $product = $item_data->get_product();
    $product_name = $product->get_name();
    $item_quantity = $item_data->get_quantity();
    $item_total = $item_data->get_total();
    $msg_for_all .= '<tr><td style="text-align:left;vertical-align:middle;border:1px solid #eee;word-wrap:break-word;color:#636363;padding:12px">'.$product_name;

    if(!empty(get_post_meta( $product->get_id(), "wpcf-product_type", true))){

			$package_content = get_post_meta( $product->get_id(), "wpcf-package_content", true);
			$package_content_prices = get_post_meta( $product->get_id(), "wpcf-prices", true);

			$package_content = explode(',',$package_content);
			$package_content_prices = explode(',',$package_content_prices);

			foreach($package_content as $keyy=>$val_pkg){
			$msg_for_all .= '<br /><span style="padding-left:15px;padding-top:5px;">'.$val_pkg.': <strong>'.wc_price(number_format($package_content_prices[$keyy],2)).'</strong></span>';
			}
		}

    $msg_for_all .= '</td><td style="text-align:left;vertical-align:middle;border:1px solid #eee;color:#636363;padding:12px">'.$item_quantity.'</td><td style="text-align:left;vertical-align:middle;border:1px solid #eee;color:#636363;padding:12px">'.wc_price(number_format( $item_total, 2 )).'</td></tr>';
    $toatal_amount = $toatal_amount + $item_total;


	}




	$msg_for_all .= '<tr><th colspan="2" style="text-align:left;border-top-width:4px;color:#636363;border:1px solid #e5e5e5;padding:12px">Subtotal:</th><td  style="text-align:left;border-top-width:4px;color:#636363;border:1px solid #e5e5e5;padding:12px">'.wc_price(number_format($toatal_amount,2)).'</td></tr>';
	$msg_for_all .= '<tr><th colspan="2" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px">Shipping:</th><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px">'.wc_price(number_format($order_data['shipping_total'],2)).'</td></tr>';
	$msg_for_all .= '<tr><th colspan="2" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px">State Tax:</th><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px">'.wc_price(number_format($order_data['total_tax'],2)).'</td></tr>';
	$msg_for_all .= '<tr><th colspan="2" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px">Payment method:</th><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px">'.$order_data['payment_method_title'].'</td></tr>';
	$msg_for_all .= '<tr><th colspan="2" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px">Total:</th><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px">'.$total_formated.'</td></tr><tbody></table>';

	$msg_for_all .= '<table  cellspacing="0" bgcolor="#f7f7f7" cellpadding="0" style="width:600px;vertical-align:top" border="0"><tbody><tr><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px" valign="top" width="50%"><h3 style="color:#a60311;display:block;font-family:Helvetica Neue,Helvetica,Roboto,Arial,sans-serif;font-size:16px;font-weight:bold;line-height: 124%;margin:16px 0 8px;text-align:left;">Billing Address</h3><p style="color:#3c3c3c;font-family:Helvetica Neue;,Helvetica,Roboto,Arial,sans-serif;margin:0 0 16px">'.$order_data['billing']['first_name'].' '.$order_data['billing']['last_name'];if(!empty($order_data['billing']['company'])){$msg_for_all .= '<br>';} $msg_for_all .=$order_data['billing']['company'].'<br><span style="text-decoration:none;color:#000;">'.$order_data['billing']['address_1'];if(!empty($order_data['billing']['address_2'])){$msg_for_all .='<br><span style="text-decoration:none;color:#000;">'.$order_data['billing']['address_2'].'</span>';}$msg_for_all .='<br>'.$order_data['billing']['city'].'<br>'.$order_data['billing']['state'].',&nbsp;'.$order_data['billing']['postcode'].'<br>'.$order_data['billing']['country'].'<br><span style="text-decoration:none;color:#000;">'.$order_data['billing']['email'].'</span><br>'.$order_data['billing']['phone'].'</p></td><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px"><h3  style="color:#a60311;display:block;font-family:Helvetica Neue,Helvetica,Roboto,Arial,sans-serif;font-size:16px;font-weight:bold;line-height: 124%;margin:16px 0 8px;text-align:left;">Shipping Address</h3><p style="color:#3c3c3c;font-family:Helvetica Neue;,Helvetica,Roboto,Arial,sans-serif;margin:0 0 16px">'.$order_data['shipping']['first_name'].' '.$order_data['shipping']['last_name'];if(!empty($order_data['shipping']['company'])){$msg_for_all .= '<br>';} $msg_for_all .=$order_data['shipping']['company'].'<br><span style="text-decoration:none;color:#000;">'.$order_data['shipping']['address_1'];if(!empty($order_data['shipping']['address_2'])){$msg_for_all .='<br><span style="text-decoration:none;color:#000;">'.$order_data['shipping']['address_2'].'</span>';}$msg_for_all .='<br>'.$order_data['shipping']['city'].'<br>'.$order_data['shipping']['state'].',&nbsp;'.$order_data['shipping']['postcode'].'<br>'.$order_data['shipping']['country'].'<br><span style="text-decoration:none;color:#000;">'.$order_data['shipping']['email'].'</span><br>'.$order_data['shipping']['phone'].'</p></td></tr><tbody></table>';
	$msg .= $msg_for_all;
    $msg .= '<tr><td align="center" valign="top" style="padding:20px 0;"><a href="'.get_site_url().'">Texas Notary Solutions</a></td></tr></table>';
    $admin_subject = '[Texas Notary Solutions] New customer order '.($order_no).' - '.date('F-d-Y',strtotime($date_created));
     $msg_admin = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:13px;line-height:1.4;background:#fff;"><tbody><tr><td align="center" valign="top"><table width="600px" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding-top:20px;"><tr><td align="center" style="padding-bottom:20px;"><a href="'.get_site_url().'"><img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /></a></td></tr><tr><td align="center" valign="top">
     <table width="600" border="0" cellspacing="0" cellpadding="10" bgcolor="#f7f7f7" style="-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);">
     <tr><td><table width="100%" border="0" cellspacing="0" cellpadding="10"><tr>
     <td align="left" colspan="2"></td></tr>
     <tr>
     <td valign="top">
     <p>Order #'.$order_no.'</p>';
     /*$msg_admin .= 'You have received an order from '.$current_user->user_firstname.' '.$current_user->user_lastname.'.The order is as follows';*/
	$msg_admin .= $msg_for_all;
    $msg_admin .= '<tr><td align="center" valign="top" style="padding:20px 0;"><a href="'.get_site_url().'">Texas Notary Solutions</a></td></tr></table></td></tr></tbody></table>';

	$retval_user = wp_mail($current_user->user_email,$subject,$msg,$header);
	$retval_admin = wp_mail(get_option( 'admin_email' ),$admin_subject,$msg_admin,$header);

}

// Mail Format To Single Users
function mailToPackageUser($order_no,$total_formated,$date_created,$order_type){

	global $wpdb;
	$order = wc_get_order( $order_no );
	$order_data = $order->get_data();

	$from = 'Texas Notary Solutions <info@texasnotarysolutions.com>';
	$header  = "From: $from" . "\r\n";
	$header .= "MIME-Version: 1.0" . "\r\n";
	$header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$subject = "Your Texas Notary Solutions order receipt from ".date('F-d-Y',strtotime($date_created));
	$current_user = wp_get_current_user();
	$msg = '<!DOCTYPE html><html><head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="x-apple-disable-message-reformatting"> <title>Emailer</title> <style> #outlook a{padding:0;} body{width:100% !important; background-color:#fff;-webkit-text-size-adjust:none; -ms-text-size-adjust:none;margin:0 !important; padding:0 !important;}  .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} ol li {margin-bottom:15px;} img{height:auto; line-height:100%; outline:none; text-decoration:none;} #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;} p {margin: 1em 0;} h1, h2, h3, h4, h5, h6 {color:#222222 !important; font-family:Arial, Helvetica, sans-serif; line-height: 100% !important;} table td {border-collapse:collapse;} .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;} .im {color:black;} div[id="tablewrap"] { width:100%; max-width:600px!important; } table[class="fulltable"], td[class="fulltd"] { max-width:100% !important; width:100% !important; height:auto !important; } @media screen and (max-device-width: 430px), screen and (max-width: 430px) {p, table, table tr, table td {font-size:16px !important;} td[class=emailcolsplit]{ width:100%!important; float:left!important; padding-left:0!important; max-width:430px !important; } td[class=emailcolsplit] img { margin-bottom:20px !important; } } </style> </head> <body style="padding:0"> <table id="backgroundTable" width="100%" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:14px;line-height:1.4;background:#fff;height:auto !important; margin:0; padding:0; width:100% !important;"> <tr> <td align="center" style="padding-bottom:20px;"> <a href="'.get_site_url().'"> <img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /> </a> </td> </tr> <tr> <td align="center" valign="top" width="100%"> <div id="tablewrap" style="width:100% !important; max-width:600px !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; padding:0 !important;"> <table id="contenttable" width="600" align="center" cellpadding="20" cellspacing="0" border="0" style="background-color:#f7f7f7; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:600px !important;-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);"> <tr> <td colspan="3" width="100%"> <table width="100%" border="0" cellspacing="0" cellpadding="10"> <tr> <td width="100%" align="left"> <h4 style="font-size:21px; margin:0 !important; margin-bottom:15px !important;text-transform:capitalize;">Hi '.$current_user->user_firstname.',</h4> <p  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-bottom:0; padding:0; font-weight:normal;">Thank you for your business! Please find your receipt below.</p> </td> </tr> <tr> <td width="100%" valign="top"><p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;"><strong>Order #</strong>'.$order_no.'</p>';
      if($order_data['status'] == 'on-hold'){
		 $msg .= '<p  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">Your order is on-hold until we confirm payment has been received. Your order details are shown below for your reference</p>';
	  }
     if($order_type == 1){

		/*global $wpdb;
		$sql_insert11 = "insert into wp_single_user_package_detail set o_id = '".$order_no."',user_id = '".get_current_user_id()."'";
		$result_insert11 = $wpdb->query($sql_insert11);
		if(count($result_insert11)<2){
		 $msg .= '<p  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;"><strong>Note : </strong> We\'re thrilled you chose TNS for your notary needs. As a token of our appreciation, use the coupon code XXXXXXXXXXX to get 10% off your next order.</p>';
		} */

     $msg .= '<p  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;">Please follow the link below to complete the notary application form.<br><a style="margin-top:10px;display: inline-block !important;text-decoration: none;text-align: center;padding: 10px !important;color: #fff;background: #a60311;" href="'.get_site_url().'/notary-application-form?o_id='. base64_encode($order_no).'">Complete The Notary Application</a></p></td></tr></table></td></tr>';

     $items_o_p = wc_get_order($order_no);
        $order_itemss = $items_o_p->get_items();
		foreach($order_itemss as $item => $values) {
			if( 'plan' == get_post_meta($values['product_id'], "wpcf-product_type", true)){
				$plan_idss = $values['product_id'];
				$p_r_name = $values['name'];
			}
		}
	global $wpdb;
	$sql_insert = "insert into wp_single_user_package_detail set o_id = '".$order_no."',user_id = '".get_current_user_id()."',p_id = '".$plan_idss."',p_name = '".$p_r_name."'";
	$result_insert = $wpdb->query($sql_insert);


	function uiwc_change_role( $order_id ) {
	$order = new WC_Order($order_id);
	$user = $order->get_user();
	if( false != $user && !user_can($user, 'administrator') && !user_can($user, 'subscriber')){
	$role = 'subscriber';
	$user->set_role($role);
	}
	}
	uiwc_change_role($order_no);
	}
	if($order_type == 0){
		$orderd = new WC_Order( $order_no);
		$itemsd = $orderd->get_items();
		foreach ( $itemsd as $item ) {
		  if (get_post_meta($item['product_id'],'save_in_insurance_custom_on_checkout',true ) =="yes") {
				global $wpdb;
				$end_d = date('Y-m-d h:i:s', strtotime('+1 years'));
				$curent_d = date('Y-m-d h:i:s');
				$ip_add = $_SERVER['REMOTE_ADDR'];
				$sql_insertss = "insert into ".$wpdb->prefix."notary_insurance set o_id = '".$order_no."',u_id = '".get_current_user_id()."',p_id = '".$item['product_id']."',p_name = '".$item['name']."',currentt_date='".$curent_d."',expire_date='".$end_d."',ip_address ='".$ip_add."'";
				$wpdb->query($sql_insertss);
				$item_nm = $item['name'];
				$item_ifd = $item['product_id'];
		  }
		}
	}
    $msg_for_all .= '<tr bgcolor="#a60311"><td width="85%" style="text-align:left;color:#fff;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Product</td><td width="5%" style="text-align:left;color:#fff;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Quantity</td><td width="10%" style="text-align:left;color:#fff;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Price</td></tr>';
    $toatal_amount = 0;
    foreach ($order->get_items() as $item_id => $item_data) {
    $product = $item_data->get_product();
    $product_name = $product->get_name();
    $item_quantity = $item_data->get_quantity();
    $item_total = $item_data->get_total();
    $msg_for_all .= '<tr><td width="85%" style="text-align:left;vertical-align:middle;border:1px solid #eee;word-wrap:break-word;color:#636363;padding:12px;font-size:14px;line-height:19px;">'.$product_name;

    if(!empty(get_post_meta( $product->get_id(), "wpcf-product_type", true))){

			$package_content = get_post_meta( $product->get_id(), "wpcf-package_content", true);
			$package_content_prices = get_post_meta( $product->get_id(), "wpcf-prices", true);

			$package_content = explode(',',$package_content);
			$package_content_prices = explode(',',$package_content_prices);

			foreach($package_content as $keyy=>$val_pkg){
			$msg_for_all .= '<p style="padding-left:15px;padding-top:0px; margin-top:0;">'.$val_pkg.': <strong>'.wc_price(number_format($package_content_prices[$keyy],2)).'</strong></p>';
			}
			$msg_for_all .= '<p class="cartStandard">Discount: <strong>'. wc_price( get_post_meta($product->get_id(), "wpcf-package_discount", true)) .'</strong> </p>';
		}

    $msg_for_all .= '</td><td width="5%" style="text-align:left;vertical-align:middle;border:1px solid #eee;color:#636363;padding:12px;font-size:14px;line-height:19px;">'.$item_quantity.'</td><td width="10%" style="text-align:left;vertical-align:middle;border:1px solid #eee;color:#636363;padding:12px;font-size:14px;line-height:19px;">'.wc_price(number_format( $item_total, 2 )).'</td></tr>';
    $toatal_amount = $toatal_amount + $item_total;
	}

	$msg_for_all .= '<tr><td colspan="2" style="text-align:left;border-top-width:4px;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Subtotal:</td><td  style="text-align:left;border-top-width:4px;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">'.wc_price(number_format($toatal_amount,2)).'</td></tr>';
	$msg_for_all .= '<tr><td colspan="2" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Shipping:</td><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">'.wc_price(number_format($order_data['shipping_total'],2)).'</td></tr>';
	$msg_for_all .= '<tr><td colspan="2" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">State Tax:</td><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">'.wc_price(number_format($order_data['total_tax'],2)).'</td></tr>';
	$msg_for_all .= '<tr><td colspan="2" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Payment method:</td><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">'.$order_data['payment_method_title'].'</td></tr>';
	$msg_for_all .= '<tr><td colspan="2" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">Total:</td><td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;font-size:14px;line-height:19px;">'.$total_formated.'</td></tr>';

	$msg_for_all .= '<tr><td colspan="3" style="padding:0;" width="100%"><table  bgcolor="#f7f7f7" cellspacing="0" cellpadding="0" style="width:100%;vertical-align:top" border="0"> <tbody> <tr> <td style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;" valign="top" width="50%"> <h3 style="color:#a60311;display:block;font-family:Helvetica Neue,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height: 25px;margin:16px 0 8px;text-align:left;">Billing Address</h3> <p style="color:#3c3c3c;font-family:Helvetica Neue;,Helvetica,Roboto,Arial,sans-serif;margin:0 0 16px;;font-size:14px;line-height:19px;">'.$order_data['billing']['first_name'].' '.$order_data['billing']['last_name'];if(!empty($order_data['billing']['company'])){$msg_for_all .= '<br>';} $msg_for_all .=$order_data['billing']['company'].'<br><span style="text-decoration:none;color:#000;">'.$order_data['billing']['address_1'];if(!empty($order_data['billing']['address_2'])){$msg_for_all .='<br><span style="text-decoration:none;color:#000;">'.$order_data['billing']['address_2'].'</span>';}$msg_for_all .='<br>'.$order_data['billing']['city'].'<br>'.$order_data['billing']['state'].',&nbsp;'.$order_data['billing']['postcode'].'<br>'.$order_data['billing']['country'].'<br><span style="text-decoration:none;color:#000;">'.$order_data['billing']['email'].'</span><br>'.$order_data['billing']['phone'].'</p> </td> <td valign="top" style="text-align:left;color:#636363;border:1px solid #e5e5e5;padding:12px;" width="50%"> <h3 style="color:#a60311;display:block;font-family:Helvetica Neue,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height: 25px;margin:16px 0 8px;text-align:left;">Shipping Address</h3> <p style="color:#3c3c3c;font-family:Helvetica Neue;,Helvetica,Roboto,Arial,sans-serif;margin:0 0 16px;font-size:14px;line-height:19px;">'.$order_data['shipping']['first_name'].' '.$order_data['shipping']['last_name'];if(!empty($order_data['shipping']['company'])){$msg_for_all .= '<br>';} $msg_for_all .=$order_data['shipping']['company'].'<br><span style="text-decoration:none;color:#000;">'.$order_data['shipping']['address_1'];if(!empty($order_data['shipping']['address_2'])){$msg_for_all .='<br><span style="text-decoration:none;color:#000;">'.$order_data['shipping']['address_2'].'</span>';}$msg_for_all .='<br>'.$order_data['shipping']['city'].'<br>'.$order_data['shipping']['state'].',&nbsp;'.$order_data['shipping']['postcode'].'<br>'.$order_data['shipping']['country'].'<br><span style="text-decoration:none;color:#000;">'.$order_data['shipping']['email'].'</span><br>'.$order_data['shipping']['phone'].'</p></td></tr></tbody></table></td></tr></table></div></td></tr>';
	$msg .= $msg_for_all;
	$msg .= '<tr><td align="center" width="100%" style="padding:0;"><p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-bottom:0; padding:0; font-weight:normal;margin-top:0;">If we can be of further assistance, please let us know. We\'re here to help!</p></td></tr>';
    $msg .= '<tr><td width="100%" align="center" valign="top" style="padding:20px 0;"><a  style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-top:0; padding:0; font-weight:normal;" href="'.get_site_url().'">Texas Notary Solutions</a></td></tr></table></body></html>';

	//admin mail
    $admin_subject = '[Texas Notary Solutions] New customer order '.($order_no).' - '.date('F-d-Y',strtotime($date_created));
     $msg_admin = '<!DOCTYPE html><html> <head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Emailer</title> <style> #outlook a{padding:0;} body{width:100% !important; background-color:#fff;-webkit-text-size-adjust:none; -ms-text-size-adjust:none;margin:0 !important; padding:0 !important;}  .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} ol li {margin-bottom:15px;} img{height:auto; line-height:100%; outline:none; text-decoration:none;} #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;} p {margin: 1em 0;} h1, h2, h3, h4, h5, h6 {color:#222222 !important; font-family:Arial, Helvetica, sans-serif; line-height: 100% !important;} table td {border-collapse:collapse;} .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;} .im {color:black;} div[id="tablewrap"] { width:100%; max-width:600px!important; } table[class="fulltable"], td[class="fulltd"] { max-width:100% !important; width:100% !important; height:auto !important; } @media screen and (max-device-width: 430px), screen and (max-width: 430px) {p, table, table tr, table td {font-size:18px !important;} td[class=emailcolsplit]{ width:100%!important; float:left!important; padding-left:0!important; max-width:430px !important; } td[class=emailcolsplit] img { margin-bottom:20px !important; } } </style> </head> <body> <table id="backgroundTable" width="100%" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:14px;line-height:1.4;background:#fff;height:auto !important; margin:0; padding:0; width:100% !important;"> <tbody> <tr> <td align="center" valign="top"> <table width="600px" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding-top:20px;"> <tr> <td align="center" style="padding-bottom:20px;"> <a href="'.get_site_url().'"> <img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /> </a> </td> </tr> <tr> <td align="center" valign="top"> <div id="tablewrap" style="width:100% !important; max-width:600px !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important;"> <table id="contenttable" width="600" align="center" cellpadding="20" cellspacing="0" border="0" style="background-color:#f7f7f7; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:600px !important;-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);"> <tr> <td width="100%"> <table width="100%" border="0" cellspacing="0" cellpadding="10"> <tr> <td width="100%" valign="top" style="padding:20px 0;"> <p style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-bottom:0; padding:0; font-weight:normal;">Order #'.$order_no.'</p>';
   /* $msg_admin .= 'You have received an order from '.$current_user->user_firstname.' '.$current_user->user_lastname.'.The order is as follows';*/
	$msg_admin .= $msg_for_all;

    $msg_admin .= '</td></tr></table></td></tr><tr><td align="center" valign="top" style="padding:20px 0;"> <a style="font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:19px; margin-bottom:0; padding:0; font-weight:normal;" href="'.get_site_url().'">Texas Notary Solutions</a> </td></tr></table></div></td></tr></table></td></tr></tbody>
</table></body></html>';

	$retval_user = wp_mail($current_user->user_email,$subject,$msg,$header);
	$retval_admin = wp_mail(get_option( 'admin_email' ),$admin_subject,$msg_admin,$header);
}
/*==FUNCTIONALTY AFTER RENEW GROUP PACKAGE==*/
 if(!empty($old_order) && isset($order) && (!$order->has_status( 'failed' )) ){
	 $plan = wc_get_product($old_order[0]->plan_id);
	 $end = date('Y-m-d h:i:s', strtotime('+4 years'));
     $st = date('Y-m-d h:i:s');
     $ip = $_SERVER['REMOTE_ADDR'];
	 $parent_id = (int)$order->user_id;
	 $order_id = (int)$order->get_id();
	 $id = $old_order[0]->id;
	 $user_id = $old_order[0]->user_id;
	 $group_id = $old_order[0]->group_id;
	 /*==GET CURRENT LOGGED-IN USER DETAIL==*/
	 $current_user = wp_get_current_user();
	 $gm_email	= $current_user->user_email;
	 $gm_name = $current_user->user_firstname ;
	 /*==UPDATE ESXISTING SUBSCRIBE PACKAGE DETAIL==*/
	 $wpdb->update($table, array('order_id' => $order_id,'updated_at'=>$st,'ip_address'=>$ip),array('ID' => $id ),array('%d','%s','%s'),array('%d'));
	 $wpdb->update($expire, array('order_id' => $order_id,'order_date'=>$st,'expire_date'=>$end,'updated_at'=>$st,'ip_address'=>$ip),array('user_id'=>$user_id,'group_id'=>$group_id,'reffer_id'=>$parent_id ),array('%d','%s','%s','%s','%s'),array('%d','%d','%d'));
	 /*==MEMBER DETAIL==*/
	 $member_name = $old_order[0]->name;
	 $member_email = $old_order[0]->email;
	 /*==EMAIL FOR COMPLETE NOTARY FORM TO MEMBER AFTER RENEW PACKAGE==*/
	 $acc = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:13px;line-height:1.4;background:#fff;"><tr><td align="center" style="padding-bottom:20px;"><a href="'.get_site_url().'"><img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /></a></td></tr><tr><td align="center" valign="top">
     <table width="600" border="0" cellspacing="0" cellpadding="10" bgcolor="#f7f7f7" style="-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);">
     <tr><td><table width="100%" border="0" cellspacing="0" cellpadding="10"><tr><td align="left" colspan="2"><h4>Hi '.$member_name.',</h4></td></tr><tr><td valign="top">
     <p>Your Package has been renewed now.</p>
     <p>Your next renewal date will be '.date("F d,Y", strtotime($end)).'.</p>
     <p>Please follow the link below to complete the notary application form.<br><a style="margin-top:10px;display: inline-block !important;text-decoration: none;text-align: center;padding: 10px !important;color: #fff;background: #334667;" href="'.get_site_url().'/group-notary-application-form/?key='. base64_encode($order->get_order_number()).'">Complete The Notary Application</a></p><div class="thanks-note"><p>If you have any questions, please do not hesitate to contact us.</p> <p>We&#39;re here to help! Thank you for your business.</p></div><p>Andrew Whitfield<br>Founder</p></td>
     </tr></table></td></tr><tr><td align="center" valign="top" style="padding:20px 0;"><a href="'.get_site_url().'">Texas Notary Solutions</a></td></tr></table>';
      $for  = "Complete The Notary Application Form";
	  $header1 = "From:Texas Notary Solutions <info@texasnotarysolutions.com> \r\n";
      $header1 .= "MIME-Version: 1.0\r\n";
	  $header1 .= "Content-type: text/html\r\n";
	 // mail($member_email,$for,$acc,$header1);
	  wp_mail( $member_email, $for, $acc, $header1);
     /*==mail to group manager==*/
     $subject2 = "You have renewed your package.";
     $txt = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:13px;line-height:1.4;background:#fff;"><tr>
     <td align="center" style="padding-bottom:20px;"><a href="'.get_site_url().'"><img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /></a></td></tr><tr><td align="center" valign="top">
     <table width="600" border="0" cellspacing="0" cellpadding="10" bgcolor="#FFFFFF" style="-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);"><tr><td><table width="100%" border="0" cellspacing="0" cellpadding="10"><tr><td align="left" colspan="4"><h4>Hi '.$gm_name.',</h4><p>I am pleased to inform that you have renewed your member ' .$member_name. '. His/Her membership package details as follows:</p><p>Your next renewal date will be '.date("F d,Y", strtotime($end)).'.</p></td></tr><tr><td colspan="4"><table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee;"><tr><td width="20px"><strong>Order: </strong></td><td>#'.$order->get_id().'</td><td width="90px"><strong>Total Amount: </strong></td><td>'.$order->get_formatted_order_total().'</td></tr></table>
	 <tr><td colspan="4"><table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee;"><tr><td width="80px"><strong>Package : </strong></td><td>'.$plan->name.'</td><td><strong>Package Duration: </strong></td>
	 <td>4 Year</td></tr></table></td></tr>';
	 if(!empty($old_order)){
	 $txt.='<tr><td colspan="4">Package details: </td></tr>';
	 foreach($old_order as  $members){
	 $product_ids2 = json_decode($members->add_product);
	 if(!in_array(230,$product_ids2)){
		array_unshift($product_ids2,230,138);
	  }
     $variation_prod2 = json_decode($members->product_variations);
	 if(!empty($product_ids2)){
	 $txt.= '<tr><td width="48px"><strong>Name :</strong></td><td>'.$members->name.'</td><td width="48px"><strong>Email :</strong></td><td><span style="text-decoration:none;color:#000;">'.$members->email.'</span></td></tr>';
	 $txt.= '<tr><td colspan="4">Product details: </td></tr>';
	 $txt.= '<tr><td colspan="4"><table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee;">';
	 $txt.= '<tr><th width="15px">S.N</th><th align="left"><strong>Product Name</strong></th></tr>';
	 $j=0;foreach($product_ids2 as $item2){ $j++;
	 $item_detail2 = wc_get_product($item2);
     if(!empty($item_detail2)){
     $txt.= '<tr>';
     $txt.= '<td align="center">'.$j.'</td>';
     $txt.= '<td>'.$item_detail2->name;
     foreach( $variation_prod2 as $value2){
	 $name_count2 = count($value2->$item2);
	 $g2=0; foreach($value2->$item2 as $item_price2){ $g2++;
	 if($name_count2 == 1){
	 $txt.= '<br/> <strong> Policy </strong> : $'.ucfirst($item_price2);
	 }else {
	 if($g2==1){
	 $txt.='<br/> <strong> Color </strong> : '.ucfirst($item_price2);
	 }else{
	 $txt.= '<br/> <strong> Shape </strong> : '.ucfirst($item_price2);
	 } } } }
     $txt.= '</td></tr>';
	 } }
	 $txt.= '</table></td></tr>';
	 } }  }
     $txt.= '</table></td></tr><tr><td valign="top">';
     $txt.= '<div class="thanks-note"><p>If you have any questions, please do not hesitate to <a href="'.get_site_url().'/contact-us/">contact us</a>.</p> <p>We&#39;re here to help! Thank you for your business.</p></div><p>Andrew Whitfield<br>Founder</p></td></tr></table></td></tr><tr><td align="center" valign="top" style="padding:20px 0;"><a href="'.get_site_url().'">Texas Notary Solutions</a></td></tr></table>';
     $header = "From:Texas Notary Solutions <info@texasnotarysolutions.com> \r\n";
     $header .= "MIME-Version: 1.0\r\n";
	 $header .= "Content-type: text/html\r\n";
	wp_mail( $gm_email, $subject2, $txt, $header);
	 /*==EMAIL TO SITE ADMIN==*/
	 $admin = get_option( 'admin_email' );
     $subject3 = "Goup Package Oder Detail( #".$order->get_id().")";
     $msg = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:13px;line-height:1.4;background:#fff;"> <tr>
    <td align="center" style="padding-bottom:20px;"><a href="'.get_site_url().'"><img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /></a></td></tr><tr>
    <td align="center" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="10" bgcolor="#FFFFFF" style="-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);"><tr><td><table width="100%" border="0" cellspacing="0" cellpadding="10">
    <tr><td align="left" colspan="4"><h4>Hi Admin,</h4><p> '.$gm_name. '(Group Manager) has renewed  <strong>'.$plan->name.'</strong> package. His/Her Member  ( '.$member_name.' )  details as follows:</p></td></tr><tr><td colspan="4"><table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee;"><tr><td width="20px"><strong>Order: </strong></td><td>#'.$order->get_id().'</td><td width="90px"><strong>Total Amount: </strong></td><td>'.$order->get_formatted_order_total().'</td></tr></table></td></tr><tr><td colspan="4"><table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee;"><tr><td width="80px"><strong>Order Date : </strong></td><td>'.wc_format_datetime( $order->get_date_created() ).'</td><td width="60px"><strong>Package : </strong></td><td>'.$plan->name.'</td></tr></table></td></tr>';
	 if(!empty($old_order)){
	 $msg.='<tr><td colspan="4">Group Member(s) details: </td></tr>';
	 foreach($old_order as  $members){
	 $product_ids2 = json_decode($members->add_product);
	 if(!in_array(230,$product_ids2)){
			 array_unshift($product_ids2,230,138);
	 }
	 $variation_prod2 = json_decode($members->product_variations);
	 if(!empty($product_ids2)){
	  $msg.= '<tr><td width="48px"><strong>Name :</strong></td><td>'.$members->name.'</td><td width="48px"><strong>Email :</strong></td><td><span style="text-decoration:none;color:#000;">'.$members->email.'</span></td></tr>';
	  $msg.= '<tr><td colspan="4"><table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#ffffff" style="border:1px solid #eee;">';
	  $msg.= '<tr><th width="15px">S.N</th><th align="left"><strong>Product Name</strong></th></tr>';
	  $j=0;foreach($product_ids2 as $item2){ $j++;
	  $item_detail2 = wc_get_product($item2);
      if(!empty($item_detail2)){
      $msg.= '<tr>';
      $msg.= '<td align="center">'.$j.'</td>';
      $msg.= '<td>'.$item_detail2->name;
      foreach( $variation_prod2 as $value2){
	  $name_count2 = count($value2->$item2);
	  $g2=0; foreach($value2->$item2 as $item_price2){ $g2++;
	  if($name_count2 == 1){
	  $msg.= '<br/> <strong> Policy </strong> : $'.ucfirst($item_price2);
	  }else {
	  if($g2==1){
	  $msg.='<br/> <strong> Color </strong> : '.ucfirst($item_price2);
	  }else{
	  $msg.= '<br/> <strong> Shape </strong> : '.ucfirst($item_price2);
	  } } } }
      $msg.= '</td></tr>';
	  } }
	  $msg.= '</table></td></tr>';
	  } } }
      $msg.= '</table></td></tr></table></td></tr><tr><td align="center" valign="top" style="padding:20px 0;"><a href="'.get_site_url().'">Texas Notary Solutions</a></td></tr></table>';
      $header2 = "From:Texas Notary Solutions <info@texasnotarysolutions.com> \r\n";
      $header2 .= "MIME-Version: 1.0\r\n";
	  $header2 .= "Content-type: text/html\r\n";
	 // mail($admin,$subject3,$msg,$header2);
	  wp_mail( $admin, $subject3, $msg, $header2 );

 }

 /*==DELETE ALL ACTIVE CUSTOM SESSION VARIABLE AFTER RENEWAL PACKAGE==*/
 if(!empty($_SESSION['renew_id'])){
	  $par = $_SESSION['renew_id'];
	  /*==DELETE TEMPORARY DATA FROM TEMPORARY TABLE==*/
	  $delete_res = $wpdb->delete( $renew , array( 'ID' => $par ) );
      unset($_SESSION['renew_id']);
}

