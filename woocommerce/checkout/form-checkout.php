<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if(isset($_SESSION['redirecthomecount'])){
	unset($_SESSION['redirecthomecount']);
}
?>
<?php  if(!empty($_SESSION) && !empty($_SESSION['parent'])){ ?>
<div class="row bs-wizard" style="border-bottom:0;">                
		<div class="col-xs-3 bs-wizard-step complete">
		  <div class="text-center bs-wizard-stepnum">Group Manager Details</div>
		  <div class="progress1"><div class="progress-bar1"></div></div>
		  <a href="#" class="bs-wizard-dot"></a>
		</div>
		
		<div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
		  <div class="text-center bs-wizard-stepnum">Add Member</div>
		  <div class="progress1"><div class="progress-bar1"></div></div>
		  <a href="#" class="bs-wizard-dot"></a>
		</div>
		
		<div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
		  <div class="text-center bs-wizard-stepnum">Cart</div>
		  <div class="progress1"><div class="progress-bar1"></div></div>
		  <a href="#" class="bs-wizard-dot"></a>
		</div>
		
		<div class="col-xs-3 bs-wizard-step active"><!-- active -->
		  <div class="text-center bs-wizard-stepnum">Checkout</div>
		  <div class="progress1"><div class="progress-bar1"></div></div>
		  <a href="#" class="bs-wizard-dot"></a>
		</div>
	</div>
<?php } ?>
<?php
  global $wpdb;
  $table_name = $wpdb->prefix . "group_tempuser";
wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>

<?php  if(!empty($_SESSION)){
	  $parent = $_SESSION['parent'];
	  }
      
  if(!empty($parent)){
				  
  $query1 ='SELECT * FROM '.$table_name.' WHERE id = '.$parent;	
  $manager = $wpdb->get_results($query1);
 }	  
	  
?>
<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>
	
	<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
	
	<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>
	
	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>

<?php if(!is_user_logged_in()){
 if(!empty($manager)){ ?>
 <script>	
	jQuery(window).ready(function($){
		console.log("<?php echo $manager;?>");
		var name = "<?php echo $manager[0]->name;?>";
		var last_name = "<?php echo $manager[0]->last_name;?>";
		var mail = "<?php echo $manager[0]->email;?>";
		$('#billing_first_name').val(name);
		$('#billing_last_name').val(last_name);
		$('#billing_email').val(mail);
		
		});
	
</script>		
<?php } } ?>

