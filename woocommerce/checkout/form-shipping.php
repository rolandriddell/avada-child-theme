<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<?php /*<link rel='stylesheet' id='avada-IE-fontawesome-css'  href="<?php echo get_site_url(); ?>/wp-content/themes/Avada/includes/lib/assets/fonts/fontawesome/font-awesome.css?ver=5.2.2" type='text/css' media='all' />*/ ?>
<script>
	jQuery( document ).ready(function() {
		jQuery("#tax_exempt_id_field").prepend("<p>Sales tax-exempt entities include government organizations (E.g., police departments, DA's office, towns, schools, and charities) that have been designated as sales tax exempt by the IRS. All other customers are required to pay sales tax.</p>");
  jQuery("#showExempt").click(function(){
  	// if(! jQuery('#tax_exempt_checkbox').is(':checked')) {
	  // jQuery('#tax_exempt_checkbox').prop('checked', true);	
	  //  jQuery('#tax_exempt_id_field').slideDown('slow');
  	//   jQuery('.continue-checkout').css('pointer-events', 'none');
  	// }
  	// else
  	// {
  	// 	jQuery('#tax_exempt_checkbox').prop('checked', false);	
  	// 	jQuery('#tax_exempt_id_field').slideUp('slow');
  	//     jQuery('input#tax_exempt_id').val('');
  	//     jQuery('.continue-checkout').css('pointer-events', 'auto');
  	//     jQuery( document.body ).trigger( 'update_checkout' );

  	// }
    //jQuery("#tax_exempt_id_field").slideToggle();
    jQuery("#showExempt .fa-plus-circle").toggle();
    jQuery("#showExempt .fa-minus-circle").toggle();
});
  jQuery("#showShipping").click(function(){
  	if(! jQuery('#ship-to-different-address-checkbox').is(':checked')) {
	  jQuery('#ship-to-different-address-checkbox').prop('checked', true);	
  	}
  	else
  	{
  		jQuery('#ship-to-different-address-checkbox').prop('checked', false);	
  	}
  	jQuery(".shipping_address").slideToggle();
  	jQuery("#showShipping .fa-plus-circle").toggle();
    jQuery("#showShipping .fa-minus-circle").toggle();
  });
  });
</script>
<div class="woocommerce-shipping-fields">
	<?php if ( true === WC()->cart->needs_shipping_address() ) : ?>
		<h3 id="showShipping"><i class="fa fa-plus-circle"></i> <i class="fa fa-minus-circle" style="display:none"></i> Ship to a different address?</h3>
		
		<h3 id="ship-to-different-address" style="height: 0px; width: 0px;overflow: hidden;margin: 0px;padding: 0">
			<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
				<input id="ship-to-different-address-checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" /> <span><?php _e( 'Ship to a different address?', 'woocommerce' ); ?></span>
			</label>
		</h3>

		<div class="shipping_address">

			<?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

			<div class="woocommerce-shipping-fields__field-wrapper">
				<?php
					$fields = $checkout->get_checkout_fields( 'shipping' );

					foreach ( $fields as $key => $field ) {
						if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
							$field['country'] = $checkout->get_value( $field['country_field'] );
						}
						woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
					}
				?>
			</div>

			<?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>
			<div class="addressCheckbox"> <label><input type="checkbox" name="shipchangetobill" id="shipchangetobill"> Change shipping address with billing address.</label></div>
		</div>

	<?php endif; ?>

</div>
<div class="woocommerce-additional-fields clearfix that">
	<?php do_action( 'woocommerce_before_order_notes', $checkout ); ?>

	<?php if ( apply_filters( 'woocommerce_enable_order_notes_field', 'yes' === get_option( 'woocommerce_enable_order_comments', 'yes' ) ) ) : ?>

		<?php if ( ! WC()->cart->needs_shipping() || wc_ship_to_billing_address_only() ) : ?>

			<h3><?php _e( 'Additional information', 'woocommerce' ); ?></h3>

		<?php endif; ?>

		<div class="woocommerce-additional-fields__field-wrapper">
			<?php foreach ( $checkout->get_checkout_fields( 'order' ) as $key => $field ) : ?>
				<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
			<?php endforeach; ?>
		</div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>
</div>

<script>
	jQuery(document).ready(function(){
		if(jQuery("#shipping_first_name").val()==jQuery("#billing_first_name").val() && jQuery("#shipping_last_name").val()==jQuery("#billing_last_name").val() && jQuery("#shipping_company").val()==jQuery("#billing_company").val() && jQuery("#shipping_address_1").val()==jQuery("#billing_address_1").val() && jQuery("#shipping_address_2").val()==jQuery("#billing_address_2").val() && jQuery("#shipping_city").val()==jQuery("#billing_city").val() && jQuery("#shipping_state").val()==jQuery("#billing_state").val() && jQuery("#shipping_postcode").val()==jQuery("#billing_postcode").val())
		{
			jQuery('#shipchangetobill').attr('checked', true);
			jQuery('#shipchangetobill').attr("disabled", true);
		}
		jQuery("#shipchangetobill").change(function() {
		    if(this.checked) {
		    	jQuery("#shipping_first_name").val(jQuery("#billing_first_name").val());
				jQuery("#shipping_last_name").val(jQuery("#billing_last_name").val());
				jQuery("#shipping_company").val(jQuery("#billing_company").val());
				jQuery("#shipping_address_1").val(jQuery("#billing_address_1").val());
				jQuery("#shipping_address_2").val(jQuery("#billing_address_2").val());
				jQuery("#shipping_city").val(jQuery("#billing_city").val());
				jQuery("#shipping_state").val(jQuery("#billing_state").val());
				jQuery("#shipping_postcode").val(jQuery("#billing_postcode").val());
				jQuery('#shipchangetobill').attr("disabled", true);
		        
		    }
		});
		jQuery(".shipping_address .input-text ").keyup(function(){
			jQuery('#shipchangetobill').attr('checked', false);
			jQuery('#shipchangetobill').removeAttr("disabled");
		});
		jQuery(".shipping_address .state_select ").keyup(function(){
			jQuery('#shipchangetobill').attr('checked', false);
			jQuery('#shipchangetobill').removeAttr("disabled");
		});

	});
</script>

