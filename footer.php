<?php
/**
* The footer template.
*
* @package Avada
* @subpackage Templates
*/
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php do_action( 'avada_after_main_content' ); ?>
</div>  <!-- fusion-row -->
</main>  <!-- #main -->
<?php do_action( 'avada_after_main_container' ); ?>
<?php global $social_icons; ?>
<?php if ( false !== strpos( Avada()->settings->get( 'footer_special_effects' ), 'footer_sticky' ) ) : ?>
</div>
<?php endif; ?>
<?php
/**
* Get the correct page ID.
*/
$c_page_id = Avada()->fusion_library->get_page_id();
?>
<?php
/**
* Only include the footer.
*/
?>
<?php if ( ! is_page_template( 'blank.php' ) ) : ?>
<?php $footer_parallax_class = ( 'footer_parallax_effect' == Avada()->settings->get( 'footer_special_effects' ) ) ? ' fusion-footer-parallax' : ''; ?>
<div class="fusion-footer<?php echo esc_attr( $footer_parallax_class ); ?>">
<?php
/**
* Check if the footer widget area should be displayed.
*/
$display_footer = get_post_meta( $c_page_id, 'pyre_display_footer', true );
?>
<?php if ( ( Avada()->settings->get( 'footer_widgets' ) && 'no' !== $display_footer ) || ( ! Avada()->settings->get( 'footer_widgets' ) && 'yes' === $display_footer ) ) : ?>
<?php $footer_widget_area_center_class = ( Avada()->settings->get( 'footer_widgets_center_content' ) ) ? ' fusion-footer-widget-area-center' : ''; ?>
<footer role="contentinfo" class="clear fusion-footer-widget-area fusion-widget-area<?php echo esc_attr( $footer_widget_area_center_class ); ?>">
	<div class="fusion-row">
		<div class="fusion-columns fusion-columns-<?php echo esc_attr( Avada()->settings->get( 'footer_widgets_columns' ) ); ?> fusion-widget-area">
			<?php
			/**
			* Check the column width based on the amount of columns chosen in Theme Options.
			*/
			$footer_widget_columns = Avada()->settings->get( 'footer_widgets_columns' );
			$footer_widget_columns = ( ! $footer_widget_columns ) ? 1 : $footer_widget_columns;
			$column_width = ( '5' == Avada()->settings->get( 'footer_widgets_columns' ) ) ? 2 : 12 / $footer_widget_columns;
			?>
			<?php
			/**
			* Render as many widget columns as have been chosen in Theme Options.
			*/
			?>
			<?php for ( $i = 1; $i < 7; $i++ ) : ?>
			<?php if ( $i <= Avada()->settings->get( 'footer_widgets_columns' ) ) : ?>
			<div class="fusion-column<?php echo ( Avada()->settings->get( 'footer_widgets_columns' ) == $i ) ? ' fusion-column-last' : ''; ?> col-lg-<?php echo esc_attr( $column_width ); ?> col-md-<?php echo esc_attr( $column_width ); ?> col-sm-<?php echo esc_attr( $column_width ); ?>">
				<?php if ( function_exists( 'dynamic_sidebar' ) && dynamic_sidebar( 'avada-footer-widget-' . $i ) ) : ?>
				<?php
				/**
				* All is good, dynamic_sidebar() already called the rendering.
				*/
				?>
				<?php endif; ?>
			</div>
			<?php endif; ?>
			<?php endfor; ?>
			<div class="fusion-clearfix"></div>
			</div> <!-- fusion-columns -->
			</div> <!-- fusion-row -->
			</footer> <!-- fusion-footer-widget-area -->
			<?php endif; // End footer wigets check. ?>
			<?php
			/**
			* Check if the footer copyright area should be displayed.
			*/
			$display_copyright = get_post_meta( $c_page_id, 'pyre_display_copyright', true );
			?>
			<?php if ( ( Avada()->settings->get( 'footer_copyright' ) && 'no' !== $display_copyright ) || ( ! Avada()->settings->get( 'footer_copyright' ) && 'yes' === $display_copyright ) ) : ?>
			<?php $footer_copyright_center_class = ( Avada()->settings->get( 'footer_copyright_center_content' ) ) ? ' fusion-footer-copyright-center' : ''; ?>
			<footer id="footer" class="fusion-footer-copyright-area<?php echo esc_attr( $footer_copyright_center_class ); ?>">
				<div class="fusion-row">
					<div class="fusion-copyright-content">
						<?php
						/**
						* Footer Content (Copyright area) avada_footer_copyright_content hook.
						*
						* @hooked avada_render_footer_copyright_notice - 10 (outputs the HTML for the Theme Options footer copyright text)
						* @hooked avada_render_footer_social_icons - 15 (outputs the HTML for the footer social icons)..
						*/
						do_action( 'avada_footer_copyright_content' );
						?>
						</div> <!-- fusion-fusion-copyright-content -->
						</div> <!-- fusion-row -->
						</footer> <!-- #footer -->
						<?php endif; // End footer copyright area check. ?>
						<?php
						// Displays WPML language switcher inside footer if parallax effect is used.
						if ( defined( 'ICL_SITEPRESS_VERSION' ) && 'footer_parallax_effect' === Avada()->settings->get( 'footer_special_effects' ) ) {
							global $wpml_language_switcher;
							$slot = $wpml_language_switcher->get_slot( 'statics', 'footer' );
							if ( $slot->is_enabled() ) {
								echo $wpml_language_switcher->render( $slot ); // WPCS: XSS ok.
							}
						}
						?>
						</div> <!-- fusion-footer -->
						<?php endif; // End is not blank page check. ?>
						</div> <!-- wrapper -->
						<?php
						/**
						* Check if boxed side header layout is used; if so close the #boxed-wrapper container.
						*/
						$page_bg_layout = ( $c_page_id ) ? get_post_meta( $c_page_id, 'pyre_page_bg_layout', true ) : 'default';
						?>
						<?php if ( ( ( 'Boxed' === Avada()->settings->get( 'layout' ) && 'default' === $page_bg_layout ) || 'boxed' === $page_bg_layout ) && 'Top' !== Avada()->settings->get( 'header_position' ) ) : ?>
						</div> <!-- #boxed-wrapper -->
						<?php endif; ?>
						<?php if ( ( ( 'Boxed' === Avada()->settings->get( 'layout' ) && 'default' === $page_bg_layout ) || 'boxed' === $page_bg_layout ) && 'framed' === Avada()->settings->get( 'scroll_offset' ) && 0 !== intval( Avada()->settings->get( 'margin_offset', 'top' ) ) ) : ?>
						<div class="fusion-top-frame"></div>
						<div class="fusion-bottom-frame"></div>
						<?php if ( 'None' !== Avada()->settings->get( 'boxed_modal_shadow' ) ) : ?>
						<div class="fusion-boxed-shadow"></div>
						<?php endif; ?>
						<?php endif; ?>
						<a class="fusion-one-page-text-link fusion-page-load-link"></a>
						<?php if (is_front_page()) : ?>
						<div class="modal fade videoModal" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header clearfix">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									</div>
									<div class="modal-body">
										<iframe class="embed-responsive-item" frameborder="0" src="https://player.vimeo.com/video/318610335" height="360" width="100%"></iframe>
									</div>
								</div>
							</div>
						</div>
						<?php endif; ?>
						<script>
						<?php /* if ( is_user_logged_in() ) { ?>
						jQuery('.login-logout a').attr("href","<?php echo wp_logout_url(get_permalink());  ?>").val('Logout');
						<?php } else { ?>
						jQuery('.login-logout a').attr("href","<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>").val('Login');
						<?php } */ ?>
						jQuery(document).ready(function() {
						jQuery("#billing_email").attr('autocomplete', 'off');
						jQuery("#account_password").attr('autocomplete', 'off');
						jQuery('body').click(function(){
						jQuery('.embed-responsive-item').attr('src', jQuery('.embed-responsive-item').attr('src'));
						});
						//	jQuery(".selectShape").prepend("<select><option value='Rectangular'>Rectangular</option><option value='Circle'>Circle</option></select><select><option value='Black'>Black</option><option value='Blue'>Blue</option></select>");
						});
						jQuery(function($){
						$("#billing_postcode").on("blur",function(){
						var phone_regex=/^[1-9][0-9]{4}$/;
						var data=$(this).val();
						if(!data.match(phone_regex))
						{
						//alert("hii i m wrong");
						$(this).next(".erro_fon").remove();
						$(this).after("<span style='color:red;' class='erro_fon'>Please fill correct zip</span>");
						$('.continue-checkout').css('pointer-events', 'none');
						}
						else
						{
						$('.continue-checkout').css('pointer-events', 'auto');
						}
						});
						$("#billing_postcode").on("change",function(){
						$(this).next(".erro_fon").remove();
						});
						$("#billing_postcode").on("click",function(){
						$(this).next(".erro_fon").remove();
						});
						$("#billing_phone").on("blur",function(){
						var phone_regex=/^(1\s?)?((\([0-9]{3}\))|[0-9]{3})[\s\-]?[\0-9]{3}[\s\-]?[0-9]{4}$/;
						var data=$(this).val();
						//alert("hii i m ryt");
						if(!data.match(phone_regex))
						{
						$(this).addClass("removeGreen");
						$(this).next(".erro_fon").remove();
						//alert("hii i m wrong");
						$(this).after("<span style='color:red;' class='erro_fon'>Please fill correct phone</span>");
						$('.continue-checkout').css('pointer-events', 'none');
						}
						else
						{
						$('.continue-checkout').css('pointer-events', 'auto');
						}
						});
						$("#billing_phone").on("change",function(){
						$(this).next(".erro_fon").remove();
						});
						$("#billing_phone").on("click",function(){
						$(this).next(".erro_fon").remove();
						});
						$("#tax_exempt_id").on("change",function(){
						$(this).next(".erro_fon").remove();
						});
						/*$("a[data-name='col-1'],a[data-name='col-2'],a[data-name='col-3']").on('click',function(){
						});*/
						<?php if( is_user_logged_in() ) {
							$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
						?>
						$(".login-logout a").attr("href","<?php echo str_replace('amp;','',wp_logout_url($actual_link.'/dashboard')); ?>");
						$(".login-logout a").text("Logout");
						<?php }?>
						});
						</script>
						<?php wp_footer(); ?>
						<script>
							jQuery( ".woocommerce-MyAccount-orders" ).wrap( "<div class='tableWrap'></div>" );
						</script>
						<script>
							if (window.DeviceOrientationEvent) {
								//window.addEventListener('orientationchange', function() { location.reload(); }, false);
							}
							function myexFunction(){
							/* jQuery("html, body").animate({ scrollTop: 0 }, "slow");*/
							jQuery(".woocommerce-info .showlogin").trigger("click");
							}
						</script>
						<?php
						global $current_user;
						get_currentuserinfo();
						if ($current_user->ID == ''){  ?>
						<script>
							jQuery(document).on("click","#billing_email",function(){
								jQuery("#spanalreadyalertmsg").remove();
							});
						jQuery(document).on("blur","#billing_email",function(){
						jQuery(".mailoader").css("display", "block");
						jQuery("#spanalreadyalertmsg").remove();
						var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
						var billing_email_data = jQuery('#billing_email').val();
						console.log(billing_email_data);
						setTimeout(function(){
						jQuery.ajax({
						url : ajaxurl,
						type : 'post',
						data : {
						action : 'billing_email_email',
						bdata : billing_email_data
						},
						async:false,
						success : function( response ) {
						count=response;
						jQuery(".mailoader").css("display", "none");
						if(count!=0)
						{
						jQuery( "#billing_email" ).after( "<span class='exist_email_login' id='spanalreadyalertmsg'>This email is already registered. <a href='#' onclick='myexFunction()' class='showlogin scrolltopexistemail'>Click here to login</a> or use a different email address</span> " );
						return false;
						}
						}
						});
						},1);
						});
						jQuery("#billing_email").click(function(){
						jQuery( ".exist_email_login" ).remove();
						});
						</script>
						<?php } ?>
						<script async="false">
							jQuery("#btnAddProfile").click(function(){
							var textval= jQuery("#btnAddProfile").html();
							if(textval=="Read More")
							{
								jQuery("#btnAddProfile").html("Read Less");
							}
							else
							{
								jQuery("#btnAddProfile").html("Read More");
							}
							});
							jQuery(document).ready(function() {
								jQuery('html').click(function(){
									jQuery('header .notification .dropdown-menu').slideUp(300);
								})
								jQuery('header .notification').click(function(e){
									e.stopPropagation();
								});
								jQuery('#notification').click(function(){
									jQuery('header .notification .dropdown-menu').slideToggle(300);
								});
							jQuery(".ptsRows .tooltipstered").removeAttr( "title" );
							jQuery(".g-recaptcha").attr("data-callback", "onReCaptchaSuccess");
							if(typeof jQuery.datepicker != "undefined"){
									jQuery( "#expiration-date" ).datepicker({
										dateFormat : 'mm-dd-yy',
										changeMonth : true,
										changeYear : true,
										yearRange: '1970:-0',
										//maxDate: '-18y'
									});
								}
								jQuery(".div_tog").click(function(){
									jQuery(this).val("processing...");
									var id=jQuery(this).attr('id');
									jQuery("#submit_"+id).css("display","block");
									jQuery("#edit_"+id).css("display","none");
								});
								jQuery(".member_change_password").click(function(){
									jQuery(this).val("processing...");
									var id=jQuery(this).attr('id');
									jQuery("#change_"+id).css("display","block");
									jQuery("#edit_"+id).css("display","none");
								});
								jQuery(".can_class,.member_edit,.password_edit").click(function(){
									var id=jQuery(this).attr('id');
									jQuery("#submit_"+id).css("display","none");
									jQuery("#change_"+id).css("display","none");
									jQuery("#edit_"+id).css("display","block");
								});
								jQuery(".can_class").click(function(){
									var id=jQuery(this).attr('id');
									jQuery("#edit_"+id+" .div_tog").val("edit");
									jQuery("#edit_"+id+" .member_change_password").val("Change Password");
								});
								jQuery(".password_edit").click(function(){
						var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
						var id=jQuery(this).attr('id');
						var new_password=jQuery('.pass_'+id).val();
						var con_password=jQuery('.con_'+id).val();
						if(new_password!='' && con_password==new_password)
						{
						jQuery.ajax({
						url : ajaxurl,
						type : 'post',
						data : {
						action : 'edit_member_password',
						id : id,
						password : new_password
						},
						success : function( response ) {
						if(response=="Mail send succesfully to member for new password.")
						{
						/*jQuery(".new_email"+id).html(new_email);*/
						jQuery("#edit_"+id+" .member_change_password").val("Password Changed");
						jQuery("#edit_"+id+" .member_change_password").off("click");
						}
						else
						{
						jQuery("#edit_"+id+" .member_change_password").val("Password Not Changed.");
						}
						}
						});
						}
						else
						{
						jQuery("#edit_"+id+" .member_change_password").val("Change Password");
						if(new_password==''){
						alert("Enter new password first.");
						}
						else if(new_password!=con_password)
						{
						alert("Password and Confirmpassword didn't matched");
						}
						}
						});
						jQuery(".member_edit").click(function(){
						var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
						var id=jQuery(this).attr('id');
						var new_email=jQuery('.email_'+id).val();
						jQuery("#submit_"+id).css("display","none");
						jQuery("#edit_"+id).css("display","block");
						if(new_email!='')
						{
						jQuery.ajax({
						url : ajaxurl,
						type : 'post',
						data : {
						action : 'edit_member_email',
						id : id,
						email : new_email
						},
						success : function( response ) {
						if(response=="Member email successfully updated.")
						{
						jQuery(".new_email"+id).html(new_email);
						jQuery("#edit_"+id+" .div_tog").val("Email Updated");
						jQuery("#edit_"+id+" .div_tog").off("click");
						}
						else
						{
						jQuery("#edit_"+id+" .div_tog").val("Not Available.");
						}
						}
						});
						}
						else
						{
						jQuery("#edit_"+id+" .div_tog").val("edit");
						alert("Enter new mail id first.");
						}
						});
						jQuery(".member_resend").click(function(){
						jQuery(this).val("Sending....");
						var respode_val="";
						var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
						var id=jQuery(this).attr('id');
						jQuery.ajax({
						url : ajaxurl,
						type : 'post',
						data : {
						action : 'resend_member_email',
						id : id
						},
						context: this,
						success : function( response ) {
						respode_val=response;
						jQuery(this).val("Message Sent!");
						}
						});
						});
						});
						var HEADER_HEIGHT = 0; // Height of header/menu fixed if exists
						var isIOS = /iPhone|iPad|iPod/i.test(navigator.userAgent);
						var grecaptchaPosition;
						var isScrolledIntoView = function (elem) {
						var elemRect = elem.getBoundingClientRect();
						var isVisible = (elemRect.top - HEADER_HEIGHT >= 0 && elemRect.bottom <= window.innerHeight);
						return isVisible;
						};
						if (isIOS) {
						var recaptchaElements = document.querySelectorAll('.g-recaptcha');
						window.addEventListener('scroll', function () {
						Array.prototype.forEach.call(recaptchaElements, function (element) {
						if (isScrolledIntoView(element)) {
						grecaptchaPosition = document.documentElement.scrollTop || document.body.scrollTop;
						}
						});
						}, false);
						}
						var onReCaptchaSuccess = function () {
						if (isIOS && grecaptchaPosition !== undefined) {
						window.scrollTo(0, grecaptchaPosition);
						}
						};
						</script>
						<script>
							var focusWhatever = function (response) {
								$("html, body").animate({ scrollTop: $("#wpcf7-f43-p61-o1").offset().top }, "slow");
							};
							var onloadCallback = function () {
								var widget = grecaptcha.render(document.getElementById("re-captcha"), {
									'sitekey' : "6LeYnS8UAAAAAFvryPBMSr3Nl4xEREQsKYZm6FMws",
									'theme': "light",
									'callback' : focusWhatever
								});
							}
							jQuery(document).ready(function(){
						jQuery("#billing_email").after("<span class='mailoader' style='display:none'><img src='<?php echo get_stylesheet_directory_uri(); ?>/images/loader.gif' alt='' /></span>");
						jQuery('#unsetcookiecartchange').click(function(){
						jQuery.cookie("gpackagesbtn", null, {expires: -1, path: '/' });
						});
						jQuery(".woocommerce-side-nav.woocommerce-checkout-nav li, .continue-checkout").on('click',function(e){
						var bbilling_first_name= jQuery('#billing_first_name').val();
						var bbilling_last_name= jQuery('#billing_last_name').val();
						var bbilling_address_1= jQuery('#billing_address_1').val();
						var bbilling_postcode= jQuery('#billing_postcode').val();
						var bbilling_phone= jQuery('#billing_phone').val();
						var bbilling_email= jQuery('#billing_email').val();
						var bbilling_city= jQuery('#billing_city').val();
						//var baccount_password= jQuery('#account_password').val();
						var bbilling_state= jQuery('#billing_state').val();
						var chackval=0;
						jQuery.ajax({
						url : "<?php echo admin_url('admin-ajax.php'); ?>",
						type : 'post',
						data : {
						action : 'billing_email_email',
						bdata : bbilling_email
						},
						success : function( response ) {
						count=response;
						jQuery(".mailoader").css("display", "none");
						if(count!=0)
						{
						jQuery('#spanalreadyalertmsg').remove();
						jQuery( "#billing_email" ).after( "<span class='exist_email_login' id='spanalreadyalertmsg'>This email is already registered. <a href='#' onclick='myexFunction()' class='showlogin scrolltopexistemail'>Click here to login</a> or use a different email address</span>" );
						jQuery('#billing_email').css({"border-color": "#a7001f"});
						chackval=1;
						}
						}
						});
						if(bbilling_first_name=="")
						{
						jQuery('#billing_first_name').css({"border-color": "#a7001f"});
						chackval=1;
						}
						if(bbilling_last_name=="")
						{
						jQuery('#billing_last_name').css({"border-color": "#a7001f"});
						chackval=1;
						}
						if(bbilling_address_1=="")
						{
						jQuery('#billing_address_1').css({"border-color": "#a7001f"});
						chackval=1;
						}
						if(bbilling_postcode=="")
						{
						jQuery('#billing_postcode').css({"border-color": "#a7001f"});
						chackval=1;
						}
						if(bbilling_phone=="")
						{
						jQuery('#billing_phone').css({"border-color": "#a7001f"});
						chackval=1;
						}
						if(bbilling_email=="")
						{
						jQuery('#billing_email').css({"border-color": "#a7001f"});
						chackval=1;
						}
						if(jQuery('#account_password').length>0)
						{
						var baccount_password= jQuery('#account_password').val();
						if(baccount_password=="")
						{
						jQuery('#account_password').css({"border-color": "#a7001f"});
						chackval=1;
						}
						}
						if(bbilling_state=="")
						{
						jQuery('#billing_state').parent('.woocommerce-input-wrapper').css({"border": "1px solid","border-color": "#a7001f"});
						jQuery('#billing_state').css({"border-color": "#a7001f"});
						chackval=1;
						}
						if(bbilling_city=="")
						{
						jQuery('#billing_city').css({"border-color": "#a7001f"});
						chackval=1;
						}
						if(chackval==1)
						{

						e.preventDefault();

						}
						});
						jQuery("#billing_first_name,#billing_last_name,#billing_address_1,#billing_postcode,#billing_phone,#billing_email,#billing_city,#billing_state").on('click',function(){
						jQuery(this).css({"border-color": "#d2d2d2"});
						});
						jQuery("#billing_state").on('change',function(){
						jQuery('#billing_state').next(".select2").css({"border": "0px solid","border-color": "#a7001f"});
						});
						var data = {
						action: 'is_user_logged_in'
						};
						jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", data, function(response) {
						if(response == 'no') {
						jQuery(".headrenew").attr("href", "/packages/");
						}
						});
						});
						jQuery("dl.variation dd").after("<br>");
						jQuery("a[data-name='order_review']").click(function(){
						jQuery("dl.variation dd").after("<br>");
						});
						jQuery(document).ready(function($) {
						$(window).load(function(){
						$('#preloader').fadeOut(900,function(){$(this).remove();});
						});
						});
						</script>
						<?php
						/**
					* Echo the scripts added to the "before </body>" field in Theme Options.
					* The 'space_body' setting is not sanitized.
					* In order to be able to take advantage of this,
					* a user would have to gain access to the database
					* in which case this is the least on your worries.
					*/
					echo Avada()->settings->get( 'space_body' ); // WPCS: XSS ok.
					?>
					<?php /*<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/validation_js/jquery.fancybox.min.js"></script>*/ ?>
					<script src="<?php echo get_stylesheet_directory_uri(); ?>/retina.js"></script>
					<?php
						if ( is_page( array( 'plan', 'contact-us', 'about-us' ))) {
					?>
						<!--Start of Zendesk Chat Script-->
						<script type="text/javascript">
						window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
						d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
						_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
						$.src="https://v2.zopim.com/?37TQ7uwS5d3KKBz1S4sT8YTWSHEBkH2M";z.t=+new Date;$.
						type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");

						$zopim(function() {
							$zopim.livechat.button.show()();
						});
						</script>
						<!--End of Zendesk Chat Script-->
					<?php }?>
					<?php
						if ( !is_user_logged_in()) {
					?>
					<!-- BEGIN PRIVY ASYNCHRONOUS WIDGET CODE
					<script type='text/javascript'>
					   var _d_site = _d_site || 'E3C04027819423860783319E';
					   (function(p, r, i, v, y) {
					     p[i] = p[i] || function() { (p[i].q = p[i].q || []).push(arguments) };
					     v = r.createElement('script'); v.async = 1; v.src = 'https://widget.privy.com/assets/widget.js';
					     y = r.getElementsByTagName('script')[0]; y.parentNode.insertBefore(v, y);
					   })(window, document, 'Privy');
					</script>
					<!-- END PRIVY ASYNCHRONOUS WIDGET CODE -->
					<?php }?>
				</body>
			</html>
