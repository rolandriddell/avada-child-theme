<?php
/* Template Name: CronJobScheduler */
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
//Get User Name
function getUserName($name_id){
	$user_name = get_user_by( 'id', $name_id ); 
	return ($user_name->first_name.' '.$user_name->last_name);
}
//Get Email id
function getEmail($name_id){
	$user_name = get_user_by( 'id', $name_id );
	return ($user_name->user_email);
}
//Get Email Content
$msg = '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="padding-top:20px;font-family: Arial, Helvetica, sans-serif;font-size:13px;line-height:1.4;background:#fff;"><tr><td align="center" style="padding-bottom:20px;"><a href="'.get_site_url().'"><img border="0" src="'.get_site_url().'/wp-content/uploads/2017/09/logo.png" alt="Texas Notary Solutions" title="Texas Notary Solutions" /></a></td></tr><tr><td align="center" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="10" bgcolor="#f7f7f7" style="-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); -moz-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20); box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.20);">';

$msg_footer = '</table></td></tr><tr><td align="center" valign="top" style="padding:20px 0;"><a href="'.get_site_url().'">Texas Notary Solutions</a></td></tr></table>';



//Notary E/O Insurance Renewal
insuranceRenewal($msg,$msg_footer);
function insuranceRenewal($msg,$msg_footer){
	global $wpdb;
	$sql_IR = "select * from wp_notary_insurance";
	$result_IR = $wpdb->get_results($sql_IR);

	foreach($result_IR as $val){
		$datetime1 = new DateTime(date('Y-m-d h:i:s'));
		$datetime2 = new DateTime($val->expire_date);
		$difference = $datetime1->diff($datetime2);

		//echo $difference->days.'<br>';
		if(($difference->invert == 0) && ($difference->y == 0) && ($difference->days == 30 || $difference->days == 2)){
			$header  = "From: Texas Notary Solutions <info@texasnotarysolutions.com> \n";
			$header .= "MIME-Version: 1.0" . "\r\n";
			$header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$subject = "Notary E/O Insurance Renewal ".date('F-d-Y h:i:s');
			
			$msg .= '<tr><td><table width="100%" border="0" cellspacing="0" cellpadding="10"><tr>
		    <td align="left" colspan="1"><h2>Hi '.getUserName($val->u_id).',</h2>
		    <p style="margin-bottom:0;font-size:16px;">Thank you for your business! Please find your receipt below.</p>
		    <p>It\'s nearly time to renew your notary E/O policy.We handle this by merely sending you an invoice for the insurance renewal or you can simply click the purchase button below to renew your policy as is.Renewing early ensures that there is no lapse in your coverage.</p>
		    <a style="margin-top:10px;display: inline-block;text-decoration: none;text-align: center;padding: 10px;color: #fff;background: #334667;" href="'.get_site_url().'?insurance_plan&insurance_id='.base64_encode($val->id).'&user_id='.base64_encode($val->u_id).'&p_id='.base64_encode($val->p_id).'">Purchase</a></td></tr></table>';
		 
			$msg .= $msg_footer;

		    $retval_user = wp_mail(getEmail($val->u_id),$subject,$msg,$header);
		   // var_dump($retval_user);
		}
	}
}
//Midway Through Your Notary Commission
midwayNotaryCommission($msg,$msg_footer);
function midwayNotaryCommission($msg,$msg_footer){
	global $wpdb;
	$sql_notary = "select email_address,first_name,last_name,expire_datee from wp_notary_form where status = 2";
	$result_notary = $wpdb->get_results($sql_notary);
	foreach($result_notary as $val){
		$datetime1 = new DateTime(date('Y-m-d h:i:s'));
		$datetime2 = new DateTime($val->expire_datee);
		$difference = $datetime1->diff($datetime2);
		//echo $difference->y.'<br>';
		if( $difference->invert == 0 && $difference->y == 2 && $difference->d == 0){
			$header  = "From: Texas Notary Solutions <info@texasnotarysolutions.com> \n";
			$header .= "MIME-Version: 1.0" . "\r\n";
			$header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
			$subject = "Notary E/O Insurance Renewal ".date('F-d-Y h:i:s');

			$msg .='<tr><td><table width="100%" border="0" cellspacing="0" cellpadding="10"><tr>
			<td align="left" colspan="1"><h2>Hi '.$val->first_name.',</h2>
			<p>We\'re just checking in to ensure that you\'re still in compliance with the notary law.</p>
			<p>You\'re at the halfway mark of your notary commission.If you\'v moved during the past two years or if your name has changed,you need to inform the secretary of state.<a href="https://www.state.gov/secretary" style="text-decoration: none;">Click here</a> to go to their site.</p>
			<p>Should you need anything else, please <a href="'.get_site_url().'/contact-us" style="text-decoration: none;">contact us</a>.We\'re here to help!.</p>
			</td></tr></table>';

			$msg .= $msg_footer;
			$retval_user = wp_mail($val->email_address,$subject,$msg,$header);
		}
	}
}
//Your Notary Anniversary
notaryAnniversary($msg,$msg_footer);
function notaryAnniversary($msg,$msg_footer){
	global $wpdb;
	$sql_notary = "select email_address,first_name,last_name,expire_datee from wp_notary_form where status = 2";
	$result_notary = $wpdb->get_results($sql_notary);
	foreach($result_notary as $val){
		$datetime1 = new DateTime(date('Y-m-d h:i:s'));
		$datetime2 = new DateTime($val->expire_datee);
		$difference = $datetime1->diff($datetime2);
		//echo $difference->days.'<br>';
		if( $difference->invert == 0 && $difference->y == 1 && $difference->d == 3 ){

				$header  = "From: Texas Notary Solutions <info@texasnotarysolutions.com> \n";
				$header .= "MIME-Version: 1.0" . "\r\n";
				$header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$subject = "Notary Annivversary ".date('F-d-Y h:i:s');

				$msg .='<tr><td><table width="100%" border="0" cellspacing="0" cellpadding="10"><tr>
				<td align="left" colspan="1"><h2>Hi '.$val->first_name.',</h2>
				<p>My how time files.You\'ve got one year left until you need to renew your notary commission.We\'ll remind you the moment you can renew your notary commission.</p>
				<p>Here are some things to keep in mind in your last year:</p>
				<p>If your name or address has changed since you were commission as a notray, <a href="https://www.state.gov/secretary" style="text-decoration: none;">you must let the Secretary of State know.</a></p>
				<p>If you\'re doing a ton of notarization, you may want to look into <a href="'.get_site_url().'/store" style="text-decoration: none;">iNotary</a>.It\'s a notary journal for your smart device and computer.With iNotary,you can get unlimited notary journal entries.</p>
				<p>If you find that you\'re not using your notary commission, you can resign at any time.</p>
				<p><a href="'.get_site_url().'/" style="margin-top: 10px;display: inline-block;text-decoration: none;text-align: center;padding: 10px;color: #fff;background: #334667;font-weight: normal;">Click here to learn how</a></p>
				</td></tr></table>';

				$msg .= $msg_footer;
				$retval_user = wp_mail($val->email_address,$subject,$msg,$header);
		}
	}
}
//Your One-Year Notary Anniversary
oneYearNotaryAnniversary($msg,$msg_footer);
function oneYearNotaryAnniversary($msg,$msg_footer){
	global $wpdb;
	$sql_notary = "select email_address,first_name,last_name,expire_datee from wp_notary_form where status = 2";
	$result_notary = $wpdb->get_results($sql_notary);
	foreach($result_notary as $val){
		$datetime1 = new DateTime(date('Y-m-d h:i:s'));
		$datetime2 = new DateTime($val->expire_datee);
		$difference = $datetime1->diff($datetime2);

		if( $difference->invert == 0 && $difference->days == 367 ){
				$header  = "From: Texas Notary Solutions <info@texasnotarysolutions.com> \n";
				$header .= "MIME-Version: 1.0" . "\r\n";
				$header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$subject = "Notary Annivversary ".date('F-d-Y h:i:s');

				$msg .='<tr><td><table width="100%" border="0" cellspacing="0" cellpadding="10"><tr>
				<td align="left" colspan="1"><h2>Hi '.$val->first_name.',</h2>
				<p>Happy Annivversary!</p>
				<p>By now,you\'ve done more than a handful of notarizations with your notary commission.Did you know that you can use your skill to make some spending money after hours and on the weekends?</p>
				<p>Just because you\'re a notary at home.If you become a remote notary, you can make $25 per notarization just by sitting behind your computer.Not bad for what amounts to about five minutes worth of work.We\'ll show you how in this <a href="'.get_site_url().'" style="text-decoration: none;">newsletter</a>.</p>
				<p>If we can be of further assistance, please <a href="'.get_site_url().'/contact-us" style="text-decoration: none;">contact us</a>. We\'re here to help.</p>
				</td></tr></table>';

				$msg .= $msg_footer;
				$retval_user = wp_mail($val->email_address,$subject,$msg,$header);
		}
	}
}
//Time to Renew your Notary Commission
renewNotaryCommission($msg,$msg_footer);
function renewNotaryCommission($msg,$msg_footer){
	global $wpdb;
	$sql_notary = "select email_address,first_name,last_name,expire_datee from wp_notary_form where status = 2";
	$result_notary = $wpdb->get_results($sql_notary);
	foreach($result_notary as $val){
		$datetime1 = new DateTime(date('Y-m-d h:i:s'));
		$datetime2 = new DateTime($val->expire_datee);
		$difference = $datetime1->diff($datetime2);
		if( $difference->invert == 0 && $difference->days == 90 ){
				$header  = "From: Texas Notary Solutions <info@texasnotarysolutions.com> \n";
				$header .= "MIME-Version: 1.0" . "\r\n";
				$header .= "Content-type:text/html;charset=UTF-8" . "\r\n";
				$subject = "Time To Renew Your Notary Commission ".date('F-d-Y h:i:s');

				$msg .='<tr><td><table width="100%" border="0" cellspacing="0" cellpadding="10"><tr>
				<td align="left" colspan="1"><h2>Hi '.$val->first_name.',</h2>
				<p>Your Notary commission is about three months away from expiring.You can renew your notary commission up to 90 days before it expires.</p>
				<p>You can begin the renewal process on the <a href="'.get_site_url().'" style="text-decoration: none;">TNS website</a>.</p>
				<p>When you renew your notary commission, you will need to purchase a new notary stamp (because the commission expiration date on the stamp will be outdated) and surety bond.If you have entries remmaining in your notary journal, you may continue to use it.</p>
				<p>If you choose to allow notary commission to lapse, you still must follow the procedures for resignation.Accordingly, you need to send your notary journal the county clerk and recorder and destroy your notary.Checkout this newsletter for more information.</p>
				<p>If we can be of further assistance, please <a href="'.get_site_url().'/contact-us" style="text-decoration: none;">contact us</a>.We\'re here to help.</p>
				</td></tr></table>';

				$msg .= $msg_footer;
				$retval_user = wp_mail($val->email_address,$subject,$msg,$header);
		}
	}
}
//Notary - Two Months Along
notaryTwoMonthsAlong($msg,$msg_footer);
function notaryTwoMonthsAlong($msg,$msg_footer){
	global $wpdb;
	global $woocommerce;
	$sql_user = "select ID,user_status from wp_users where user_status = 0";
	$result_notary = $wpdb->get_results($sql_user);
	foreach($result_notary as $val){
			$customer_orders = get_posts( array(
			'meta_key'    => '_customer_user',
			'meta_value'  => $val->ID,
			'post_type'   => 'shop_order',
			'post_status' => array_keys( wc_get_order_statuses() ),
			'numberposts' => -1
			));
			if(!empty($customer_orders[0]->ID)){
				$order = wc_get_order( $customer_orders[0]->ID );

				$order->order_date;
				$items = $order->get_items();
				echo '<pre>';
				echo $items[0]->name;
				print_r($items);
				exit;
			}
	}
}
?>
